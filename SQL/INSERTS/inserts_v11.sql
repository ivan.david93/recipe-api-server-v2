INSERT INTO `recipedb`.`category_ingredient_a` (`name_category_ingredient_a`) VALUES ('frescos');
INSERT INTO `recipedb`.`category_ingredient_a` (`name_category_ingredient_a`) VALUES ('mercearia salgada');
INSERT INTO `recipedb`.`category_ingredient_a` (`name_category_ingredient_a`) VALUES ('lacticinios');


INSERT INTO `recipedb`.`category_ingredient_b` (`name_category_ingredient_b`, `category_ingredient_a_id`) VALUES ('talho', '2');
INSERT INTO `recipedb`.`category_ingredient_b` (`name_category_ingredient_b`, `category_ingredient_a_id`) VALUES ('peixaria', '2');
INSERT INTO `recipedb`.`category_ingredient_b` (`name_category_ingredient_b`, `category_ingredient_a_id`) VALUES ('arroz', '3');
INSERT INTO `recipedb`.`category_ingredient_b` (`name_category_ingredient_b`, `category_ingredient_a_id`) VALUES ('natas e chantilly', '4');
INSERT INTO `recipedb`.`category_ingredient_b` (`name_category_ingredient_b`, `category_ingredient_a_id`) VALUES ('legumes', '2');


INSERT INTO `recipedb`.`measure` (`name_measure`, `initials_measure`) VALUES ('unidades', 'und');
INSERT INTO `recipedb`.`measure` (`name_measure`, `initials_measure`) VALUES ('gramas', 'gr');

INSERT INTO `recipedb`.`ingredient` (`ingredient_name`, `category_ingredient_b_id`, `measure_id`) VALUES ('arroz agulha', '4', '2');
INSERT INTO `recipedb`.`ingredient` (`ingredient_name`, `category_ingredient_b_id`, `measure_id`) VALUES ('pato', '2', '1');
INSERT INTO `recipedb`.`ingredient` (`ingredient_name`, `category_ingredient_b_id`, `measure_id`) VALUES ('dourada', '3', '1');
INSERT INTO `recipedb`.`ingredient` (`ingredient_name`, `category_ingredient_b_id`, `measure_id`) VALUES ('Nata Mimosa Teriyaki', '5', '1');

INSERT INTO `recipedb`.`recipe` (`rating`, `difficulty`, `create_date`, `update_date`, `number_of_ratings`, `directions`, `name_recipe`, `prep_time`) VALUES ('5', '3', '1985-02-06', '1985-02-06', '20', '1.\nParta o pato em bocados grosseiros, junte 1 cebola cortada em pedaços de 2cm e os vinhos e deixe cozer cerca de 1hora. Retire, arrefeça e desfie o pato, reservando o caldo onde foi cozinhado.\n\n2.\nColoque uma caçarola ao lume e aloure a cebola e o alho, picados, em azeite. Junte o bacon e o pato. De seguida refresque com o sumo da laranja, adicione a Molho de Nata Mimosa para Carne e envolva.\n\n3.\nJunte o arroz e de seguida junte o caldo reservado. Tempere com sal e pimenta, tape e deixe cozinhar cerca de 16 minutos. Num pirex faça camadas alternadas de arroz e pato. No topo cubra com arroz e com rodelas de chouriço. Leve ao forno a 180ºC cerca de 10 minutos até ficar dourado.', 'Arroz de Pato com Bacon e Laranja', '90');
INSERT INTO `recipedb`.`recipe` (`rating`, `difficulty`, `create_date`, `update_date`, `number_of_ratings`, `directions`, `name_recipe`, `prep_time`) VALUES ('4', '5', '1934-12-12', '1954-10-22', '30', '1.\nComece por temperar o salmão com sal, pimenta e sumo de um limão. Alterne em espetos de madeira, cubos de salmão com fatias finas de limão, lima e alho francês. Regue com azeite.\n\n2.\nColoque as espetadas num tabuleiro de forno forrado com papel vegetal e leve a cozinhar no forno pré-aquecido a 200°C. Vá virando as espetadas até estarem cozinhadas.\n\n3.\nAqueça o Molho de Nata Mimosa Teriyaki e regue as espetada, polvilhando com as sementes de sésamo. Sirva com uma salada de rúcula e cebola roxa.', 'Espetada com Salmão', '180');
INSERT INTO `recipedb`.`recipe` (`rating`, `difficulty`, `create_date`, `update_date`, `number_of_ratings`, `directions`, `name_recipe`, `prep_time`) VALUES ('2', '1', '2007-10-21', '2007-10-21', '10', '1.\nComece por separar as gemas das claras para taças diferentes. Na taça com as gemas, misture o pato, o tomate, o Molho Bechamel Mimosa e o cebolinho picado. Misture tudo muito bem e tempere com sal e pimenta a gosto.\n\n2.\nBata as claras em castelo e envolva cuidadosamente com o preparado anterior. Unte formas de soufflé com Manteiga Mimosa sem sal e polvilhe com pão ralado.\n\n3.\nLeve ao forno a cozinhar durante 15 minutos ou até o topo do soufflé estar dourado e bem insuflado. Sirva logo de seguida acompanhado de uma salada fresca.', 'Soufflé de Pato, Tomate e Cebolinho', '100');
INSERT INTO `recipedb`.`recipe` (`rating`, `difficulty`, `create_date`, `update_date`, `number_of_ratings`, `directions`, `name_recipe`, `prep_time`) VALUES ('4', '6', '2017-12-30', '2007-12-30', '15', '1. Ligue o forno a 180° C.\n2. Lave o frango, enxugue-o com papel de cozinha e pincele-o com o tempero.\n3. No interior, coloque a laranja e um limão cortados em gomos e disponha-o num tabuleiro, untado com um pouco do azeite.\n4. Leve ao forno cerca de 30 minutos.\n5. Corte o ananás em cubos, descasque as cebolinhas, envolva bem e tempere com o sal.\n6. Coloque esta mistura no tabuleiro à volta do frango 30 minutos após o início da cozedura e deixe assar mais 30 minutos.\n7. Junte o sumo do restante limão com o restante azeite, adicione uma mão-cheia de tomilho e alecrim picados, tempere com um pouco de pimenta e regue a mistura de ananás e cebola com este molho.\n8. Sirva a acompanhar o frango.', 'Frango a campones', '130');





INSERT INTO `recipedb`.`recipe_has_ingredient` (`id_recipe`, `id_ingredient`, `quantity`) VALUES ('1', '2', '200');
INSERT INTO `recipedb`.`recipe_has_ingredient` (`id_recipe`, `id_ingredient`, `quantity`) VALUES ('1', '3', '1');
INSERT INTO `recipedb`.`recipe_has_ingredient` (`id_recipe`, `id_ingredient`, `quantity`) VALUES ('2', '5', '1');
INSERT INTO `recipedb`.`recipe_has_ingredient` (`id_recipe`, `id_ingredient`, `quantity`) VALUES ('3', '3', '1');


INSERT INTO `recipedb`.`user` (`id_user`) VALUES ('10');
INSERT INTO `recipedb`.`user_category` (`name_user_category`) VALUES ('NORMAL');
INSERT INTO `recipedb`.`user_category` (`name_user_category`) VALUES ('PEIXIVORO');


INSERT INTO `recipedb`.`user_has_user_category` (`id_user`, `id_user_category`) VALUES ('10', '1');
INSERT INTO `recipedb`.`recipe_has_user_category` (`id_recipe`, `id_user_category`) VALUES ('1', '1');
INSERT INTO `recipedb`.`recipe_has_user_category` (`id_recipe`, `id_user_category`) VALUES ('2', '2');
