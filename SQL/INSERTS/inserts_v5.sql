INSERT INTO `recipedb`.`category_ingredient_a` (`name_category_ingredient_a`) VALUES ('frescos');
INSERT INTO `recipedb`.`category_ingredient_a` (`name_category_ingredient_a`) VALUES ('mercearia salgada');
INSERT INTO `recipedb`.`category_ingredient_a` (`name_category_ingredient_a`) VALUES ('lacticinios');


INSERT INTO `recipedb`.`category_ingredient_b` (`name_category_ingredient_b`, `category_ingredient_a_name_category_ingredient_a`) VALUES ('talho', 'frescos');
INSERT INTO `recipedb`.`category_ingredient_b` (`name_category_ingredient_b`, `category_ingredient_a_name_category_ingredient_a`) VALUES ('peixaria', 'frescos');
INSERT INTO `recipedb`.`category_ingredient_b` (`name_category_ingredient_b`, `category_ingredient_a_name_category_ingredient_a`) VALUES ('arroz', 'mercearia salgada');
INSERT INTO `recipedb`.`category_ingredient_b` (`name_category_ingredient_b`, `category_ingredient_a_name_category_ingredient_a`) VALUES ('natas e chantilly', 'lacticinios');


INSERT INTO `recipedb`.`measure` (`name_measure`, `initials_measure`) VALUES ('unidades', 'und');
INSERT INTO `recipedb`.`measure` (`name_measure`, `initials_measure`) VALUES ('gramas', 'gr');

INSERT INTO `recipedb`.`ingredient` (`ingredient_name`, `category_ingredient_b_name_category_ingredient_b`, `measure_id_measure`) VALUES ('arroz agulha', 'arroz', '2');
INSERT INTO `recipedb`.`ingredient` (`ingredient_name`, `category_ingredient_b_name_category_ingredient_b`, `measure_id_measure`) VALUES ('pato', 'talho', '1');
INSERT INTO `recipedb`.`ingredient` (`ingredient_name`, `category_ingredient_b_name_category_ingredient_b`, `measure_id_measure`) VALUES ('dourada', 'peixaria', '1');
INSERT INTO `recipedb`.`ingredient` (`ingredient_name`, `category_ingredient_b_name_category_ingredient_b`, `measure_id_measure`) VALUES ('Nata Mimosa Teriyaki', 'natas e chantilly', '1');

INSERT INTO `recipedb`.`recipe` (`rating`, `difficulty`, `create_date`, `update_date`, `number_of_ratings`, `directions`, `name_recipe`, `prep_time`) VALUES ('5', '3', '1985-02-06', '1985-02-06', '20', '1.\nParta o pato em bocados grosseiros, junte 1 cebola cortada em pedaços de 2cm e os vinhos e deixe cozer cerca de 1hora. Retire, arrefeça e desfie o pato, reservando o caldo onde foi cozinhado.\n\n2.\nColoque uma caçarola ao lume e aloure a cebola e o alho, picados, em azeite. Junte o bacon e o pato. De seguida refresque com o sumo da laranja, adicione a Molho de Nata Mimosa para Carne e envolva.\n\n3.\nJunte o arroz e de seguida junte o caldo reservado. Tempere com sal e pimenta, tape e deixe cozinhar cerca de 16 minutos. Num pirex faça camadas alternadas de arroz e pato. No topo cubra com arroz e com rodelas de chouriço. Leve ao forno a 180ºC cerca de 10 minutos até ficar dourado.', 'Arroz de Pato com Bacon e Laranja', '90');
INSERT INTO `recipedb`.`recipe` (`rating`, `difficulty`, `create_date`, `update_date`, `number_of_ratings`, `directions`, `name_recipe`, `prep_time`) VALUES ('4', '5', '1934-12-12', '1954-10-22', '30', '1.\nComece por temperar o salmão com sal, pimenta e sumo de um limão. Alterne em espetos de madeira, cubos de salmão com fatias finas de limão, lima e alho francês. Regue com azeite.\n\n2.\nColoque as espetadas num tabuleiro de forno forrado com papel vegetal e leve a cozinhar no forno pré-aquecido a 200°C. Vá virando as espetadas até estarem cozinhadas.\n\n3.\nAqueça o Molho de Nata Mimosa Teriyaki e regue as espetada, polvilhando com as sementes de sésamo. Sirva com uma salada de rúcula e cebola roxa.', 'Espetada com Salmão', '180');



INSERT INTO `recipedb`.`recipe_has_ingredient` (`id_recipe`, `id_ingredient`, `quantity`) VALUES ('1', '2', '200');
INSERT INTO `recipedb`.`recipe_has_ingredient` (`id_recipe`, `id_ingredient`, `quantity`) VALUES ('1', '3', '1');
INSERT INTO `recipedb`.`recipe_has_ingredient` (`id_recipe`, `id_ingredient`, `quantity`) VALUES ('2', '5', '1');

INSERT INTO `recipedb`.`user` (`id_user`) VALUES ('10');
INSERT INTO `recipedb`.`user_category` (`name_user_category`) VALUES ('NORMAL');
INSERT INTO `recipedb`.`user_category` (`name_user_category`) VALUES ('PEIXIVORO');
INSERT INTO `recipedb`.`user_has_user_category` (`id_user`, `name_user_category`) VALUES ('10', 'NORMAL');


INSERT INTO `recipedb`.`recipe_has_user_category` (`id_recipe`, `name_user_category`) VALUES ('1', 'NORMAL');
INSERT INTO `recipedb`.`recipe_has_user_category` (`id_recipe`, `name_user_category`) VALUES ('2', 'PEIXIVORO');
