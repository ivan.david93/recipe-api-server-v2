INSERT INTO `recipedb`.`category_ingredient_a` (`name_category_ingredient_a`) VALUES ('Frescos');

INSERT INTO `recipedb`.`recipe` (`rating`, `difficulty`, `create_date`, `update_date`, `number_of_ratings`, `directions`, `name_recipe`, `prep_time`) VALUES ('5', '3', '2017-05-16', '2017-05-16', '4', '1.\nParta o pato em bocados grosseiros, junte 1 cebola cortada em pedaços de 2cm e os vinhos e deixe cozer cerca de 1hora. Retire, arrefeça e desfie o pato, reservando o caldo onde foi cozinhado.\n\n2.\nColoque uma caçarola ao lume e aloure a cebola e o alho, picados, em azeite. Junte o bacon e o pato. De seguida refresque com o sumo da laranja, adicione a Molho de Nata Mimosa para Carne e envolva.\n\n3.\nJunte o arroz e de seguida junte o caldo reservado. Tempere com sal e pimenta, tape e deixe cozinhar cerca de 16 minutos. Num pirex faça camadas alternadas de arroz e pato. No topo cubra com arroz e com rodelas de chouriço. Leve ao forno a 180ºC cerca de 10 minutos até ficar dourado.', 'Arroz de Pato com Bacon e Laranja', '90');

INSERT INTO `recipedb`.`category_ingredient_b` (`name_category_ingredient_b`, `id_category_ingredient_a`) VALUES ('Talho', '2');

INSERT INTO `recipedb`.`measure` (`name_measure`, `initials_measure`) VALUES ('unidades', 'und');

INSERT INTO `recipedb`.`ingredient` (`ingredient_name`, `category_ingredient_b`, `category_ingredient_a`, `id_measure`) VALUES ('pato', '3', '2', '1');