-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema recipedb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `recipedb` ;

-- -----------------------------------------------------
-- Schema recipedb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `recipedb` DEFAULT CHARACTER SET utf8 ;
USE `recipedb` ;

-- -----------------------------------------------------
-- Table `recipedb`.`RECIPE`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`RECIPE` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`RECIPE` (
  `id_recipe` INT NOT NULL AUTO_INCREMENT,
  `media` BLOB NULL,
  `rating` INT NULL,
  `difficulty` INT NULL,
  `create_date` DATE NOT NULL,
  `update_date` DATE NOT NULL,
  `number_of_rating` INT NULL,
  `directions` LONGTEXT NOT NULL,
  `name_recipe` VARCHAR(45) NOT NULL,
  `prep_time` INT NULL,
  PRIMARY KEY (`id_recipe`),
  UNIQUE INDEX `nameRecipe_UNIQUE` (`name_recipe` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recipedb`.`CATEGORY_INGREDIENT_A`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`CATEGORY_INGREDIENT_A` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`CATEGORY_INGREDIENT_A` (
  `id_category_ingredient_a` INT NOT NULL AUTO_INCREMENT,
  `name_category_ingredient_a` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_category_ingredient_a`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recipedb`.`CATEGORY_INGREDIENT_B`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`CATEGORY_INGREDIENT_B` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`CATEGORY_INGREDIENT_B` (
  `id_category_ingredient_b` INT NOT NULL AUTO_INCREMENT,
  `name_category_ingredient_b` VARCHAR(45) NOT NULL,
  `id_category_ingredient_a` INT NOT NULL,
  PRIMARY KEY (`id_category_ingredient_b`, `id_category_ingredient_a`),
  INDEX `fk_categoryIngredientB_categoryIngredientA1_idx` (`id_category_ingredient_a` ASC),
  CONSTRAINT `fk_categoryIngredientB_categoryIngredientA1`
    FOREIGN KEY (`id_category_ingredient_a`)
    REFERENCES `recipedb`.`CATEGORY_INGREDIENT_A` (`id_category_ingredient_a`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recipedb`.`MEASURE`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`MEASURE` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`MEASURE` (
  `id_measure` INT NOT NULL AUTO_INCREMENT,
  `name_measure` VARCHAR(45) NOT NULL,
  `initials_measure` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_measure`),
  UNIQUE INDEX `idMeasure_UNIQUE` (`id_measure` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recipedb`.`INGREDIENT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`INGREDIENT` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`INGREDIENT` (
  `id_ingredient` INT NOT NULL AUTO_INCREMENT,
  `ingredient_name` VARCHAR(45) NOT NULL,
  `category_ingredient_b` INT NOT NULL,
  `category_ingredient_a` INT NOT NULL,
  `id_measure` INT NOT NULL,
  PRIMARY KEY (`id_ingredient`, `category_ingredient_b`, `category_ingredient_a`, `id_measure`),
  UNIQUE INDEX `ingredientName_UNIQUE` (`ingredient_name` ASC),
  INDEX `fk_ingredient_categoryIngredientB1_idx` (`category_ingredient_b` ASC, `category_ingredient_a` ASC),
  INDEX `fk_ingredient_measure1_idx` (`id_measure` ASC),
  CONSTRAINT `fk_ingredient_categoryIngredientB1`
    FOREIGN KEY (`category_ingredient_b` , `category_ingredient_a`)
    REFERENCES `recipedb`.`CATEGORY_INGREDIENT_B` (`id_category_ingredient_b` , `id_category_ingredient_a`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ingredient_measure1`
    FOREIGN KEY (`id_measure`)
    REFERENCES `recipedb`.`MEASURE` (`id_measure`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recipedb`.`USER`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`USER` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`USER` (
  `id_user` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_user`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recipedb`.`USER_CATEGORY`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`USER_CATEGORY` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`USER_CATEGORY` (
  `id_user_category` INT NOT NULL AUTO_INCREMENT,
  `name_user_category` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_user_category`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recipedb`.`RECIPE_HAS_USERCATEGORY`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`RECIPE_HAS_USERCATEGORY` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`RECIPE_HAS_USERCATEGORY` (
  `id_recipe` INT NOT NULL,
  `id_user_category` INT NOT NULL,
  INDEX `fk_recipe_has_userCategory_userCategory1_idx` (`id_user_category` ASC),
  INDEX `fk_recipe_has_userCategory_recipe1_idx` (`id_recipe` ASC),
  CONSTRAINT `fk_recipe_has_userCategory_recipe1`
    FOREIGN KEY (`id_recipe`)
    REFERENCES `recipedb`.`RECIPE` (`id_recipe`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_recipe_has_userCategory_userCategory1`
    FOREIGN KEY (`id_user_category`)
    REFERENCES `recipedb`.`USER_CATEGORY` (`id_user_category`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recipedb`.`USERCATEGORY_HAS_USER`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`USERCATEGORY_HAS_USER` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`USERCATEGORY_HAS_USER` (
  `id_user_category` INT NOT NULL,
  `id_user` INT NOT NULL,
  INDEX `fk_userCategory_has_user_user1_idx` (`id_user` ASC),
  INDEX `fk_userCategory_has_user_userCategory1_idx` (`id_user_category` ASC),
  CONSTRAINT `fk_userCategory_has_user_userCategory1`
    FOREIGN KEY (`id_user_category`)
    REFERENCES `recipedb`.`USER_CATEGORY` (`id_user_category`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_userCategory_has_user_user1`
    FOREIGN KEY (`id_user`)
    REFERENCES `recipedb`.`USER` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recipedb`.`RECIPE_HAS_INGREDIENT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`RECIPE_HAS_INGREDIENT` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`RECIPE_HAS_INGREDIENT` (
  `id_recipe` INT NOT NULL,
  `id_ingredient` INT NOT NULL,
  `quantity` INT NOT NULL,
  INDEX `fk_recipe_has_ingredient_ingredient1_idx` (`id_ingredient` ASC),
  INDEX `fk_recipe_has_ingredient_recipe1_idx` (`id_recipe` ASC),
  CONSTRAINT `fk_recipe_has_ingredient_recipe1`
    FOREIGN KEY (`id_recipe`)
    REFERENCES `recipedb`.`RECIPE` (`id_recipe`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_recipe_has_ingredient_ingredient1`
    FOREIGN KEY (`id_ingredient`)
    REFERENCES `recipedb`.`INGREDIENT` (`id_ingredient`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
