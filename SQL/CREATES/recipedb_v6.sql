-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema recipedb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `recipedb` ;

-- -----------------------------------------------------
-- Schema recipedb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `recipedb` DEFAULT CHARACTER SET utf8 ;
USE `recipedb` ;

-- -----------------------------------------------------
-- Table `recipedb`.`category_ingredient_a`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`category_ingredient_a` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`category_ingredient_a` (
  `id_category_ingredient_a` INT(11) NOT NULL AUTO_INCREMENT,
  `name_category_ingredient_a` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_category_ingredient_a`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `recipedb`.`category_ingredient_b`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`category_ingredient_b` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`category_ingredient_b` (
  `id_category_ingredient_b` INT(11) NOT NULL AUTO_INCREMENT,
  `name_category_ingredient_b` VARCHAR(45) NOT NULL,
  `id_category_ingredient_a` INT(11) NOT NULL,
  PRIMARY KEY (`id_category_ingredient_b`, `id_category_ingredient_a`),
  INDEX `fk_categoryIngredientB_categoryIngredientA1_idx` (`id_category_ingredient_a` ASC),
  CONSTRAINT `fk_categoryIngredientB_categoryIngredientA1`
    FOREIGN KEY (`id_category_ingredient_a`)
    REFERENCES `recipedb`.`category_ingredient_a` (`id_category_ingredient_a`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `recipedb`.`measure`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`measure` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`measure` (
  `id_measure` INT(11) NOT NULL AUTO_INCREMENT,
  `name_measure` VARCHAR(45) NOT NULL,
  `initials_measure` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_measure`),
  UNIQUE INDEX `idMeasure_UNIQUE` (`id_measure` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `recipedb`.`ingredient`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`ingredient` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`ingredient` (
  `id_ingredient` INT(11) NOT NULL AUTO_INCREMENT,
  `ingredient_name` VARCHAR(45) NOT NULL,
  `category_ingredient_b` INT(11) NOT NULL,
  `id_measure` INT(11) NOT NULL,
  PRIMARY KEY (`id_ingredient`, `category_ingredient_b`),
  UNIQUE INDEX `ingredientName_UNIQUE` (`ingredient_name` ASC),
  INDEX `fk_ingredient_categoryIngredientB1_idx` (`category_ingredient_b` ASC),
  INDEX `fk_ingredient_measure1_idx` (`id_measure` ASC),
  CONSTRAINT `fk_ingredient_categoryIngredientB1`
    FOREIGN KEY (`category_ingredient_b`)
    REFERENCES `recipedb`.`category_ingredient_b` (`id_category_ingredient_b`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ingredient_measure1`
    FOREIGN KEY (`id_measure`)
    REFERENCES `recipedb`.`measure` (`id_measure`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `recipedb`.`recipe`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`recipe` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`recipe` (
  `id_recipe` INT(11) NOT NULL AUTO_INCREMENT,
  `media` BLOB NULL DEFAULT NULL,
  `rating` INT(11) NULL DEFAULT NULL,
  `difficulty` INT(11) NULL DEFAULT NULL,
  `create_date` DATE NOT NULL,
  `update_date` DATE NOT NULL,
  `number_of_ratings` INT(11) NULL DEFAULT NULL,
  `directions` LONGTEXT NOT NULL,
  `name_recipe` VARCHAR(45) NOT NULL,
  `prep_time` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_recipe`),
  UNIQUE INDEX `nameRecipe_UNIQUE` (`name_recipe` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `recipedb`.`recipe_has_ingredient`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`recipe_has_ingredient` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`recipe_has_ingredient` (
  `id_recipe` INT(11) NOT NULL,
  `id_ingredient` INT(11) NOT NULL,
  `quantity` INT(11) NOT NULL,
  INDEX `fk_recipe_has_ingredient_ingredient1_idx` (`id_ingredient` ASC),
  INDEX `fk_recipe_has_ingredient_recipe1_idx` (`id_recipe` ASC),
  PRIMARY KEY (`id_recipe`, `id_ingredient`),
  CONSTRAINT `fk_recipe_has_ingredient_ingredient1`
    FOREIGN KEY (`id_ingredient`)
    REFERENCES `recipedb`.`ingredient` (`id_ingredient`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_recipe_has_ingredient_recipe1`
    FOREIGN KEY (`id_recipe`)
    REFERENCES `recipedb`.`recipe` (`id_recipe`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `recipedb`.`user_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`user_category` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`user_category` (
  `id_user_category` INT(11) NOT NULL AUTO_INCREMENT,
  `name_user_category` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_user_category`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `recipedb`.`recipe_has_usercategory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`recipe_has_usercategory` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`recipe_has_usercategory` (
  `id_recipe` INT(11) NOT NULL,
  `id_user_category` INT(11) NOT NULL,
  INDEX `fk_recipe_has_userCategory_userCategory1_idx` (`id_user_category` ASC),
  INDEX `fk_recipe_has_userCategory_recipe1_idx` (`id_recipe` ASC),
  PRIMARY KEY (`id_recipe`, `id_user_category`),
  CONSTRAINT `fk_recipe_has_userCategory_recipe1`
    FOREIGN KEY (`id_recipe`)
    REFERENCES `recipedb`.`recipe` (`id_recipe`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_recipe_has_userCategory_userCategory1`
    FOREIGN KEY (`id_user_category`)
    REFERENCES `recipedb`.`user_category` (`id_user_category`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `recipedb`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`user` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`user` (
  `id_user` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_user`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `recipedb`.`usercategory_has_user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`usercategory_has_user` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`usercategory_has_user` (
  `id_user_category` INT(11) NOT NULL,
  `id_user` INT(11) NOT NULL,
  INDEX `fk_userCategory_has_user_user1_idx` (`id_user` ASC),
  INDEX `fk_userCategory_has_user_userCategory1_idx` (`id_user_category` ASC),
  PRIMARY KEY (`id_user_category`, `id_user`),
  CONSTRAINT `fk_userCategory_has_user_user1`
    FOREIGN KEY (`id_user`)
    REFERENCES `recipedb`.`user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_userCategory_has_user_userCategory1`
    FOREIGN KEY (`id_user_category`)
    REFERENCES `recipedb`.`user_category` (`id_user_category`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
