-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema recipedb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `recipedb` ;

-- -----------------------------------------------------
-- Schema recipedb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `recipedb` DEFAULT CHARACTER SET utf8 ;
USE `recipedb` ;

-- -----------------------------------------------------
-- Table `recipedb`.`category_ingredient_a`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`category_ingredient_a` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`category_ingredient_a` (
  `name_category_ingredient_a` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`name_category_ingredient_a`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `recipedb`.`category_ingredient_b`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`category_ingredient_b` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`category_ingredient_b` (
  `name_category_ingredient_b` VARCHAR(45) NOT NULL,
  `category_ingredient_a_name_category_ingredient_a` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`name_category_ingredient_b`, `category_ingredient_a_name_category_ingredient_a`),
  INDEX `fk_category_ingredient_b_category_ingredient_a1_idx` (`category_ingredient_a_name_category_ingredient_a` ASC),
  CONSTRAINT `fk_category_ingredient_b_category_ingredient_a1`
    FOREIGN KEY (`category_ingredient_a_name_category_ingredient_a`)
    REFERENCES `recipedb`.`category_ingredient_a` (`name_category_ingredient_a`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `recipedb`.`measure`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`measure` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`measure` (
  `id_measure` INT NOT NULL AUTO_INCREMENT,
  `name_measure` VARCHAR(45) NOT NULL,
  `initials_measure` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_measure`),
  UNIQUE INDEX `idmeasure_UNIQUE` (`id_measure` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recipedb`.`ingredient`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`ingredient` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`ingredient` (
  `id_ingredient` INT(11) NOT NULL AUTO_INCREMENT,
  `ingredient_name` VARCHAR(45) NOT NULL,
  `category_ingredient_b_name_category_ingredient_b` VARCHAR(45) NOT NULL,
  `measure_id_measure` INT NOT NULL,
  PRIMARY KEY (`id_ingredient`),
  UNIQUE INDEX `ingredientName_UNIQUE` (`ingredient_name` ASC),
  INDEX `fk_ingredient_category_ingredient_b_idx` (`category_ingredient_b_name_category_ingredient_b` ASC),
  INDEX `fk_ingredient_measure1_idx` (`measure_id_measure` ASC),
  CONSTRAINT `fk_ingredient_category_ingredient_b`
    FOREIGN KEY (`category_ingredient_b_name_category_ingredient_b`)
    REFERENCES `recipedb`.`category_ingredient_b` (`name_category_ingredient_b`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ingredient_measure1`
    FOREIGN KEY (`measure_id_measure`)
    REFERENCES `recipedb`.`measure` (`id_measure`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `recipedb`.`recipe`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`recipe` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`recipe` (
  `id_recipe` INT NOT NULL AUTO_INCREMENT,
  `rating` INT NOT NULL,
  `difficulty` INT NOT NULL,
  `create_date` DATE NOT NULL,
  `update_date` DATE NOT NULL,
  `number_of_ratings` INT NOT NULL,
  `directions` LONGTEXT NOT NULL,
  `name_recipe` VARCHAR(45) NOT NULL,
  `prep_time` INT NOT NULL,
  `media` BLOB NULL,
  PRIMARY KEY (`id_recipe`),
  UNIQUE INDEX `id_recipe_UNIQUE` (`id_recipe` ASC),
  UNIQUE INDEX `name_recipe_UNIQUE` (`name_recipe` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recipedb`.`recipe_has_ingredient`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`recipe_has_ingredient` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`recipe_has_ingredient` (
  `id_recipe` INT NOT NULL,
  `id_ingredient` INT(11) NOT NULL,
  `quantity` INT NOT NULL,
  PRIMARY KEY (`id_recipe`, `id_ingredient`),
  INDEX `fk_recipe_has_ingredient_ingredient1_idx` (`id_ingredient` ASC),
  INDEX `fk_recipe_has_ingredient_recipe1_idx` (`id_recipe` ASC),
  CONSTRAINT `fk_recipe_has_ingredient_recipe1`
    FOREIGN KEY (`id_recipe`)
    REFERENCES `recipedb`.`recipe` (`id_recipe`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_recipe_has_ingredient_ingredient1`
    FOREIGN KEY (`id_ingredient`)
    REFERENCES `recipedb`.`ingredient` (`id_ingredient`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recipedb`.`user_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`user_category` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`user_category` (
  `name_user_category` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`name_user_category`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recipedb`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`user` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`user` (
  `id_user` INT NOT NULL,
  PRIMARY KEY (`id_user`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recipedb`.`recipe_has_user_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`recipe_has_user_category` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`recipe_has_user_category` (
  `id_recipe` INT NOT NULL,
  `name_user_category` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_recipe`, `name_user_category`),
  INDEX `fk_recipe_has_user_category_user_category1_idx` (`name_user_category` ASC),
  INDEX `fk_recipe_has_user_category_recipe1_idx` (`id_recipe` ASC),
  CONSTRAINT `fk_recipe_has_user_category_recipe1`
    FOREIGN KEY (`id_recipe`)
    REFERENCES `recipedb`.`recipe` (`id_recipe`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_recipe_has_user_category_user_category1`
    FOREIGN KEY (`name_user_category`)
    REFERENCES `recipedb`.`user_category` (`name_user_category`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recipedb`.`user_has_user_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `recipedb`.`user_has_user_category` ;

CREATE TABLE IF NOT EXISTS `recipedb`.`user_has_user_category` (
  `id_user` INT NOT NULL,
  `name_user_category` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_user`, `name_user_category`),
  INDEX `fk_user_has_user_category_user_category1_idx` (`name_user_category` ASC),
  INDEX `fk_user_has_user_category_user1_idx` (`id_user` ASC),
  CONSTRAINT `fk_user_has_user_category_user1`
    FOREIGN KEY (`id_user`)
    REFERENCES `recipedb`.`user` (`id_user`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_has_user_category_user_category1`
    FOREIGN KEY (`name_user_category`)
    REFERENCES `recipedb`.`user_category` (`name_user_category`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
