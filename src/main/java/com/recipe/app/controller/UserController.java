package com.recipe.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.recipe.app.entity.Recipe;
import com.recipe.app.entity.User;
import com.recipe.app.entity.UserCategory;
import com.recipe.app.service.UserCategoryService;
import com.recipe.app.service.UserService;

@RestController
@RequestMapping("user")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private UserCategoryService userCategoryService;

	@GetMapping("findAll")
	public ResponseEntity<List<User>> findAll() {
		List<User> list = userService.findAllUsers();
		System.out.println(" list " + list.get(0));
		return new ResponseEntity<List<User>>(list, HttpStatus.OK);
	}

	@PostMapping("findUserById")
	public ResponseEntity<User> findUserById(

			@RequestParam(value = "idUser") int idUser) {

		// System.out.println("ggggggggggggggggggggggg "+ idUser);
		User userDb = userService.findUserById(idUser);

		// System.out.println("lllllllllllllllllllllll ------->
		// "+userDb.getUserCategoryList().get(0).getNameUserCategory());

		return new ResponseEntity<User>(userDb, HttpStatus.OK);
	}

	@PostMapping("addUsertoBD")
	// @ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<User> addUser(@RequestParam(value = "userCategoryList") List<Integer> userCategoryList) {
		User newUser = null;
		try {
			newUser = userService.addUser(userCategoryList);
		} catch (Exception e) {
			return new ResponseEntity<User>(newUser, HttpStatus.SERVICE_UNAVAILABLE);
		}

		return new ResponseEntity<User>(newUser, HttpStatus.OK);
	}

	@GetMapping("addUser")
	public ResponseEntity<User> addUser() {
		// TESTEEEEE

		// TODO ao apagar user está a apagar tabem a USER_CATEGORY que está
		// associada e ao apagar aqui apaga tambem na recipe_has_usercategory
		// ou seja a user_Category desaparece !!!
		int id = 25;

		List<UserCategory> userCategoryList = userCategoryService.findAllUserCategory();
		User userNew = userService.addUser(id, userCategoryList);
		System.out.println("User create id : " + id + " = " + userNew);
		return new ResponseEntity<User>(userNew, HttpStatus.OK);
	}

	@PostMapping("updateUser")
//	public ResponseEntity<Boolean> updateUser(@RequestParam(value = "idUser") int idUser,
//			@RequestParam(value = "userCategoryList") List<UserCategory> userCategoryList) {
	public ResponseEntity<Boolean> updateUser(@RequestParam(value = "idUser") int idUser,
			@RequestParam(value = "userCategoryIdList") List<Integer> userCategoryIdList) {
		
		
		User updatedUser = userService.updateUser(idUser, userCategoryIdList);

		
		return new ResponseEntity<Boolean>(updatedUser != null ? true : false, HttpStatus.OK);
	}

	@GetMapping("deleteUserById")
	public ResponseEntity<Boolean> deleteUserById() {

		// TODO ao apagar user está a apagar tabem a USER_CATEGORY que está
		// associada e ao apagar aqui apaga tambem na recipe_has_usercategory
		// ou seja a user_Category desaparece !!!
		int id = 10;
		boolean userDelete = userService.deleteUserById(id);
		System.out.println("User delete id : " + id + " = " + userDelete);
		return new ResponseEntity<Boolean>(userDelete, HttpStatus.OK);
	}

}
