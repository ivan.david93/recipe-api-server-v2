package com.recipe.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.recipe.app.entity.Recipe;
import com.recipe.app.entity.User;
import com.recipe.app.entity.UserCategory;
import com.recipe.app.service.UserCategoryService;
import com.recipe.app.service.UserService;

@RestController
@RequestMapping("userCategory")
public class UserCategoryController {

	@Autowired
	private UserCategoryService userCategoryService;

	@PostMapping("findAll")
	public ResponseEntity<List<UserCategory>> findAllUserCategory() {
		List<UserCategory> list = userCategoryService.findAllUserCategory();
		System.out.println(" list " + list.toString());
		return new ResponseEntity<List<UserCategory>>(list, HttpStatus.OK);
	}

	@PostMapping("findUserCategoryById")
	public ResponseEntity<UserCategory> findUserCategoryById(

			@RequestParam(value = "idUserCategory") int idUserCategory) {
		
		UserCategory userCategoryDb = userCategoryService.findUserCategoryById(idUserCategory);
		System.out.println("------------------------------------------userrrr "+idUserCategory);
		// userCategory.getRecipeList();
		if (userCategoryDb != null) {
			System.out.println(" UserCategory :  " + userCategoryDb.getNameUserCategory());
			return new ResponseEntity<UserCategory>(userCategoryDb, HttpStatus.OK);
		}

		return null;
	}
	

	
	@GetMapping("findByName/{nameUserCat}")
	public ResponseEntity<UserCategory> findByName(@PathVariable("nameUserCat") String nameUserCat) {
		// nameUserCat = "NEW";
		UserCategory userCategory = userCategoryService.findByName(nameUserCat);
		// userCategory.getRecipeList();
		if (userCategory != null) {
			System.out.println(" UserCategory :  " + userCategory.getNameUserCategory());
		}

		return new ResponseEntity<UserCategory>(userCategory, HttpStatus.OK);
	}

	@GetMapping("addUserCategory/{newCategory}")
	public ResponseEntity<UserCategory> addUserCategory(@PathVariable("newCategory") String newCategory) {
		// newCategory = "VEGAN";
		UserCategory userCategory = userCategoryService.addUserCategory(newCategory);
		if (userCategory != null) {
			System.out.println(" UserCategory :  " + userCategory.getNameUserCategory());
		}

		return new ResponseEntity<UserCategory>(userCategory, HttpStatus.OK);
	}

	@GetMapping("deleteUserCategoryByName")
	public ResponseEntity<Boolean> deleteUserCategoryByName() {
		String name = "PEIXIVORO";
		boolean userCateogryDelete = userCategoryService.deleteUserCategoryByName(name);
		System.out.println("UserCategory delete name : " + name + " = " + userCateogryDelete);
		return new ResponseEntity<Boolean>(userCateogryDelete, HttpStatus.OK);
	}

	@GetMapping("updateUserCategory")
	public ResponseEntity<Boolean> updateUserCategory() {
		String nameCategoryOld = "PEIXIVORO";
		String nameNew = "NEW";
		boolean userCategoryUpdate = userCategoryService.updateUserCategory(nameCategoryOld, nameNew);
		System.out.println("userCategory Update name : " + nameNew + " = " + userCategoryUpdate);
		return new ResponseEntity<Boolean>(userCategoryUpdate, HttpStatus.OK);
	}

	@PostMapping("findUserCategoryByUserId")
	// @ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<List<UserCategory>> findUserCategoryByUserId(

			@RequestParam(value = "userId") int userId) {

		List<UserCategory> listOfUserCategory = userCategoryService.findUserCategoryByUserId(userId);

		return new ResponseEntity<List<UserCategory>>(listOfUserCategory, HttpStatus.OK);
	}

}
