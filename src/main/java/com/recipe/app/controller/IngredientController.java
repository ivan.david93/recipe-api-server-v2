package com.recipe.app.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.recipe.app.entity.CategoryA;
import com.recipe.app.entity.CategoryB;
import com.recipe.app.entity.Ingredient;
import com.recipe.app.entity.IngredientHelper;
import com.recipe.app.entity.Measure;
import com.recipe.app.entity.Recipe;
import com.recipe.app.entity.RecipeIngredient;
import com.recipe.app.entity.UserCategory;
import com.recipe.app.service.IngredientService;
import com.recipe.app.service.UserCategoryService;

@RestController
@RequestMapping("ingredient")
public class IngredientController {

	@Autowired
	private IngredientService ingredientService;
	
	@Autowired
	private UserCategoryService userCategoryService;

	// _____________________________________________________________________________________________________________
	// ___________________________________ Ingredient
	// _____________________________________________________________
	// _____________________________________________________________________________________________________________

	@GetMapping("findAllIngredients")
	public ResponseEntity<List<Ingredient>> findAllIngredients() {
		List<Ingredient> list = ingredientService.findAllIngredients();
		
		return new ResponseEntity<List<Ingredient>>(list, HttpStatus.OK);
	}

	@GetMapping("findIngredientById")
	public ResponseEntity<Ingredient> findIngredientById() {
		Ingredient ingredient = ingredientService.findIngredientById(3);
		return new ResponseEntity<Ingredient>(ingredient, HttpStatus.OK);
	}

	@GetMapping("findIngredientByName")
	public ResponseEntity<Ingredient> findIngredientByName() {

		Ingredient ingredient = ingredientService.findIngredientByName("pato");
		System.out.println("O ingrediente é : " + ingredient);

		return new ResponseEntity<Ingredient>(ingredient, HttpStatus.OK);
	}

	@GetMapping("addIngredient")
	public ResponseEntity<Ingredient> addIngredient() {

		Ingredient newIngredient = new Ingredient();
		newIngredient.setIngredientName("Frango");

		Measure measureIng = new Measure();
		measureIng.setIdMeasure(1);
		measureIng.setNameMeasure("unidades");
		measureIng.setInitialsMeasure("und");

		newIngredient.setMeasure(measureIng);

		CategoryA categA = new CategoryA();
		categA.setIdCategoryIngredientA(3);
		categA.setNameCategoryIngredientA("mercearia salgada");

		CategoryB categB = new CategoryB();
		categB.setIdCategoryIngredientB(3);
		categB.setNameCategoryIngredientB("legumazz");
		categB.setCategoryIngredientA(categA);

		newIngredient.setCategoryIngredientB(categB);

		Ingredient addNewIngredient = ingredientService.addIngredient(newIngredient);
		return new ResponseEntity<Ingredient>(addNewIngredient, HttpStatus.OK);
	}

	@GetMapping("updateIngredient")
	public void updateIngredient() {

		Ingredient newIngredient = new Ingredient();
		newIngredient.setIdIngredient(7);
		newIngredient.setIngredientName("Franga");

		Measure measureIng = new Measure();
		measureIng.setIdMeasure(1);
		measureIng.setNameMeasure("unidades");
		measureIng.setInitialsMeasure("und");

		newIngredient.setMeasure(measureIng);

		CategoryA categA = new CategoryA();
		categA.setIdCategoryIngredientA(2);
		categA.setNameCategoryIngredientA("talho");

		CategoryB categB = new CategoryB();
		categB.setIdCategoryIngredientB(2);
		categB.setNameCategoryIngredientB("frescos");
		categB.setCategoryIngredientA(categA);

		newIngredient.setCategoryIngredientB(categB);

		ingredientService.updateIngredient(newIngredient);
	}

	@GetMapping("deleteIngredient")
	public boolean deleteIngredient() {
		return ingredientService.deleteIngredient(6);
	}

	@GetMapping("findRecipesByIngredient")
	public ResponseEntity<List<RecipeIngredient>> findRecipesByIngredient() {

		List<RecipeIngredient> recipeIngredientList = ingredientService.findRecipesByIngredient(3);

		return new ResponseEntity<List<RecipeIngredient>>(recipeIngredientList, HttpStatus.OK);
	}

	@PostMapping("findRecipesByIngredients")
	public ResponseEntity<List<Recipe>> findRecipesByIngredients(
			@RequestParam(value = "ingredientsChoosenList") List<Integer> ingredientsChoosenList , @RequestParam(value = "idUser") Integer idUser) {
		
		List<UserCategory> userCatList = userCategoryService.findUserCategoryByUserId(idUser);
		
		List<Recipe> recipes = new ArrayList<Recipe>();
		// TODO LIMITAR AS RECEITAS GERADAS A 10 POSSIVEIS -- ESTE METODO E PARA
		// MUDAR
		int maxRecipes = 10;
		recipes = ingredientService.findRecipesByIngredients(ingredientsChoosenList, maxRecipes ,userCatList);

		return new ResponseEntity<List<Recipe>>(recipes, HttpStatus.OK);
	}

	@PostMapping("findIngredientstTotalByRecipesIds")
	public ResponseEntity<List<IngredientHelper>> findIngredientstTotalByRecipesIds(
			@RequestParam(value = "listIdRecipes") List<Integer> listIdRecipes) {

		List<IngredientHelper> ingredientList = new ArrayList<IngredientHelper>();

		try {
			ingredientList = ingredientService.findIngredientstTotalByRecipesIds(listIdRecipes);
		} catch (Exception e) {
			return new ResponseEntity<List<IngredientHelper>>(ingredientList, HttpStatus.SERVICE_UNAVAILABLE);
		}

		return new ResponseEntity<List<IngredientHelper>>(ingredientList, HttpStatus.OK);
	}

	@PostMapping("findIngredientsByRecipe")
	public ResponseEntity<List<IngredientHelper>> findIngredientsByRecipe(
			@RequestParam(value = "idRecipe") int idRecipe) {

		List<IngredientHelper> ingredientList = new ArrayList<IngredientHelper>();

		try {
			ingredientList = ingredientService.findIngredientsByRecipe(idRecipe);
		} catch (Exception e) {
			return new ResponseEntity<List<IngredientHelper>>(ingredientList, HttpStatus.SERVICE_UNAVAILABLE);
		}

		return new ResponseEntity<List<IngredientHelper>>(ingredientList, HttpStatus.OK);
	}

	// _____________________________________________________________________________________________________________
	// ___________________________________ CategoryA
	// _____________________________________________________________
	// _____________________________________________________________________________________________________________

	@GetMapping("findAllCategoriesA")
	public ResponseEntity<List<CategoryA>> findAllCategoriesA() {
		List<CategoryA> listCategoriesA = ingredientService.findAllCategoriesA();
		return new ResponseEntity<List<CategoryA>>(listCategoriesA, HttpStatus.OK);
	}

	@GetMapping("findCategoryAById")
	public ResponseEntity<CategoryA> findCategoryAById() {
		CategoryA categoryA = ingredientService.findCategoryAById(5);
		return new ResponseEntity<CategoryA>(categoryA, HttpStatus.OK);
	}

	@GetMapping("findCategoryAByName")
	public ResponseEntity<CategoryA> findCategoryAByName() {
		String nameCategoryA = "frescos";
		CategoryA categoryA = ingredientService.findCategoryAByName(nameCategoryA);
		return new ResponseEntity<CategoryA>(categoryA, HttpStatus.OK);
	}

	@GetMapping("addCategoryA")
	public CategoryA addCategoryA() {
		CategoryA newCategoryA = new CategoryA();
		newCategoryA.setNameCategoryIngredientA("lituania");
		CategoryA addNewCategoryA = ingredientService.addCategoryA(newCategoryA);
		return addNewCategoryA;
	}

	@GetMapping("updateCategoryA")
	public CategoryA updateCategoryA() {

		int idCategoryA = 5;
		CategoryA newCategoryA = new CategoryA();
		newCategoryA.setNameCategoryIngredientA("zeca");

		CategoryA updateCatA = ingredientService.updateCategoryA(idCategoryA, newCategoryA);

		return updateCatA;
	}

	@GetMapping("deleteCategoryA")
	public boolean deleteCategoryA() {
		boolean deleteCategA = ingredientService.deleteCategoryA(6);
		return deleteCategA;
	}

	// _____________________________________________________________________________________________________________
	// ___________________________________ CategoryB
	// _____________________________________________________________
	// _____________________________________________________________________________________________________________

	@GetMapping("findAllCategoriesB")
	public ResponseEntity<List<CategoryB>> findAllCategoriesB() {
		List<CategoryB> listCategoriesB = ingredientService.findAllCategoriesB();
		return new ResponseEntity<List<CategoryB>>(listCategoriesB, HttpStatus.OK);
	}

	@GetMapping("findCategoryBById")
	public ResponseEntity<CategoryB> findCategoryBById() {
		int idCategoryB = 2;
		CategoryB categoryB = ingredientService.findCategoryBById(idCategoryB);
		return new ResponseEntity<CategoryB>(categoryB, HttpStatus.OK);
	}

	@GetMapping("findCategoryBByName")
	public ResponseEntity<CategoryB> findCategoryBByName() {
		String nameCategoryB = "talho";
		CategoryB categoryB = ingredientService.findCategoryBByName(nameCategoryB);
		return new ResponseEntity<CategoryB>(categoryB, HttpStatus.OK);
	}

	/**
	 * tem que existir categ A
	 * 
	 * @return
	 */
	@GetMapping("addCategoryB")
	public CategoryB addCategoryB() {

		CategoryB newCategoryB = new CategoryB();
		newCategoryB.setNameCategoryIngredientB("gomas");

		CategoryA newCategoryA = new CategoryA();
		newCategoryA.setIdCategoryIngredientA(3);
		newCategoryA.setNameCategoryIngredientA("mercearia salgada");

		newCategoryB.setCategoryIngredientA(newCategoryA);

		CategoryB addCategB = ingredientService.addCategoryB(newCategoryB);
		return addCategB;

	}

	@GetMapping("updateCategoryB")
	public CategoryB updateCategoryB() {

		CategoryB newCategoryB = new CategoryB();
		newCategoryB.setIdCategoryIngredientB(6);
		newCategoryB.setNameCategoryIngredientB("legumazzz");

		CategoryA newCategoryA = new CategoryA();
		newCategoryA.setIdCategoryIngredientA(2);
		newCategoryA.setNameCategoryIngredientA("mercearia salgada");

		newCategoryB.setCategoryIngredientA(newCategoryA);

		CategoryB updateCatB = ingredientService.updateCategoryB(newCategoryB);
		return updateCatB;
	}

	@GetMapping("deleteCategoryB")
	public boolean deleteCategoryB() {
		return ingredientService.deleteCategoryB(7);
	}

	// _____________________________________________________________________________________________________________
	// ___________________________________ Measure
	// _____________________________________________________________
	// _____________________________________________________________________________________________________________

	@GetMapping("findAllMeasures")
	public ResponseEntity<List<Measure>> findAllMeasures() {
		List<Measure> listMeasures = ingredientService.findAllMeasures();
		return new ResponseEntity<List<Measure>>(listMeasures, HttpStatus.OK);
	}

	@GetMapping("findMeasureById")
	public ResponseEntity<Measure> findMeasureById() {
		Measure measure = ingredientService.findMeasureById(1);
		return new ResponseEntity<Measure>(measure, HttpStatus.OK);
	}

	@GetMapping("findMeasureByName")
	public ResponseEntity<Measure> findMeasureByName() {
		String nameMeasure = "unidades";
		Measure measure = ingredientService.findMeasureByName(nameMeasure);
		return new ResponseEntity<Measure>(measure, HttpStatus.OK);
	}

	@GetMapping("addMeasure")
	public ResponseEntity<Measure> addMeasure() {
		Measure newMeasure = new Measure();
		newMeasure.setNameMeasure("litros");
		newMeasure.setInitialsMeasure("lt");
		Measure addMeasure = ingredientService.addMeasure(newMeasure);
		return new ResponseEntity<Measure>(addMeasure, HttpStatus.OK);

	}

	@GetMapping("updateMeasure")
	public ResponseEntity<Measure> updateMeasure() {

		Measure newMeasure = new Measure();

		newMeasure.setIdMeasure(3);
		newMeasure.setNameMeasure("litras");
		newMeasure.setInitialsMeasure("lta");

		Measure updateMeasure = ingredientService.updateMeasure(newMeasure);
		return new ResponseEntity<Measure>(updateMeasure, HttpStatus.OK);
	}

	@GetMapping("deleteMeasure")
	public ResponseEntity<Boolean> deleteMeasure() {
		boolean deleteMeasure = ingredientService.deleteMeasure(3);
		return new ResponseEntity<Boolean>(deleteMeasure, HttpStatus.OK);
	}

}
