package com.recipe.app.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.recipe.app.entity.CategoryA;
import com.recipe.app.entity.CategoryB;
import com.recipe.app.entity.Ingredient;
import com.recipe.app.entity.Measure;
import com.recipe.app.entity.Recipe;
import com.recipe.app.entity.RecipeIngredient;
import com.recipe.app.entity.UserCategory;
import com.recipe.app.service.RecipeService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Controller
@RequestMapping("recipe")
public class RecipeController {

	@Autowired
	private RecipeService recipeService;

	@PostMapping("/autenticar")
	public @ResponseBody LoginResponse autenticar(@RequestBody Map<String, String> data) {
		// se user der sucesso de autenticacao no firebase chama autentricar
		System.out.println("userName: " + data.get("userName") + " password: " + data.get("password"));

		// TOKEN {token : DGDKJGMD474D48D48D7FD}
		String token = Jwts.builder().setSubject(data.get("userName")).signWith(SignatureAlgorithm.HS512, "banana")
				.setExpiration(new Date(System.currentTimeMillis() + 10 * 60 * 1000)).compact();// "454dvg5ddf4df44dfgbfd";
																								// //API
																								// jwt
																								// ger
																								// token
		return new LoginResponse(token);

	}

	private class LoginResponse {

		@JsonProperty("token")
		private String token;

		public LoginResponse(String token) {
			// TODO Auto-generated constructor stub
			this.token = token;
		}

		// public String getToken(){
		// return this.token;
		// }

	}

	// @GetMapping("findAll")
	@PostMapping("findAll")
	public ResponseEntity<List<Recipe>> findAllRecipes() {
		List<Recipe> list = recipeService.getAllRecipes();
		// System.out.println(" list " + list.get(0));
		System.out.println("---------------------  PASSOU AQUI  ----------------------------------");
		return new ResponseEntity<List<Recipe>>(list, HttpStatus.OK);
	}


	@PostMapping("findRecipesByNumOfIng")
	// @ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<List<Recipe>> findRecipesByNumOfIng(
			@RequestParam(value = "idListCategory") List<Integer> idListCategory,
			@RequestParam(value = "numOfRecipes") int numOfRecipes) {
		
		
//		System.out.println("idListCategory ---> " + idListCategory);
//		System.out.println("idListCategory ---> " + idListCategory.get(0));
//		System.out.println("idListCategory ---> " + idListCategory.get(1));
//		System.out.println("numOfRecipes   ---> " + numOfRecipes);
		List<Recipe> listOfRecipesByNum = recipeService.getRecipesByNum(idListCategory,numOfRecipes);
		
		return new ResponseEntity<List<Recipe>>(listOfRecipesByNum, HttpStatus.OK);
	}
	
	
	@PostMapping("findRecipeById")
	public ResponseEntity<Recipe> findRecipeById(@RequestParam(value = "idRecipe") int idRecipe) {
		
		Recipe recipeDB = null;
//		List<RecipeIngredient> recipeIngredients = null;
		try {
			recipeDB = recipeService.findRecipeById(idRecipe);
//			recipeIngredients = recipeService.findIngredientsByRecipe(recipeDB);
//			recipeDB.setIngredientList(recipeIngredients);
		} catch (Exception e) {
			return new ResponseEntity<Recipe>(recipeDB, HttpStatus.SERVICE_UNAVAILABLE);
		}
		
		return new ResponseEntity<Recipe>(recipeDB, HttpStatus.OK);
	}
	
	
	//TODO
	@PostMapping("findListRecipesByIds")
	public ResponseEntity<List<Recipe>> findListRecipesByIds(@RequestParam(value = "idListRecipes") List<Integer> idListRecipes) {
		System.out.println("----------------------->   "+idListRecipes.toString());
		List<Recipe> listRecipe = null;
		try {
			listRecipe = recipeService.findListRecipesByIds(idListRecipes);
			return new ResponseEntity<List<Recipe>>(listRecipe, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<Recipe>>(listRecipe, HttpStatus.SERVICE_UNAVAILABLE);

		}
	}
	
	
	@PostMapping("rateRecipe")
	public ResponseEntity<Recipe> rateRecipe(@RequestParam(value = "rating") float rating , @RequestParam(value = "idRecipe") int idRecipe ) {
		
		Recipe recipeDB = null;
//		List<RecipeIngredient> recipeIngredients = null;
		try {
			recipeDB = recipeService.rateRecipe(rating, idRecipe);
//			recipeIngredients = recipeService.findIngredientsByRecipe(recipeDB);
//			recipeDB.setIngredientList(recipeIngredients);
		} catch (Exception e) {
			return new ResponseEntity<Recipe>(recipeDB, HttpStatus.SERVICE_UNAVAILABLE);
		}
		
		return new ResponseEntity<Recipe>(recipeDB, HttpStatus.OK);
	}
	
	
	@GetMapping("isServerAlive")
	public ResponseEntity<Boolean> isServerAlive() {
		boolean isAlive = false;
		
		try {
			List<Recipe> recipes = null;
			recipes = recipeService.getAllRecipes();
			isAlive = true;
			return new ResponseEntity<Boolean>(isAlive, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Boolean>(isAlive, HttpStatus.NOT_FOUND);
		}
		
		
		
	}


	@GetMapping("findRecipeByName/{recipeName}")
	public ResponseEntity<Recipe> findRecipeByName(@PathVariable("recipeName") String recipeName) {
		// recipeName = "Arroz de Pato com Bacon e Laranja";
		Recipe recipeDb = recipeService.findRecipeByName(recipeName);
		System.out.println("Recipe find " + recipeDb);
		return new ResponseEntity<Recipe>(recipeDb, HttpStatus.OK);
	}

	@GetMapping("addRecipe")
	public ResponseEntity<Recipe> addRecipe() {

		// TODO acabar add receita
		Recipe recipe = new Recipe();

		// TODO apenas para testes CRIAR UMA RECEITA
		Date createDate = new Date();
		recipe.setCreateDate(createDate);
		recipe.setUpdateDate(createDate);
		Integer difficulty = 5;
		recipe.setDifficulty(difficulty);

		String directions = "1. Pique o chouriço e misture-o com a carne de vaca picada. Descasque e pique finamente a cebola e leve a alourar num tacho com a Vaqueiro Alho.2. Junte a carne e deixe cozinhar, mexendo até a carne perder o tom avermelhado. Adicione o tomate em bocados e o manjericão picado.3. "
				+ "Tempere com sal e pimenta, tape o tacho e deixe cozinhar cerca de 45 minutos, sobre lume muito brando, "
				+ "mexendo de vez em quando. "
				+ "4. Quinze minutos antes de terminar de cozinhar a carne leve um tacho ao lume com água abundante, temperada com sal e um fio de óleo Vaqueiro."
				+ "5. Quando a água ferver, introduza o esparguete e deixe cozer até ficar al dente. Escorra bem e sirva misturado com a carne. Enfeite com folhinhas frescas de manjericão."
				+ "6.  À parte sirva, numa tacinha, o queijo parmesão ralado.";
		recipe.setDirections(directions);

		List<RecipeIngredient> ingredientList = new ArrayList<RecipeIngredient>();
		Ingredient novoIngrediente = new Ingredient("chourico", new CategoryB("talho", new CategoryA("frescos")),
				new Measure(1, "unidades", "und"));
		RecipeIngredient newRecipeIngredient = new RecipeIngredient(recipe, novoIngrediente, 1);
		ingredientList.add(newRecipeIngredient);
		recipe.setIngredientList(ingredientList);

		String media = null;
		recipe.setMedia(media);

		String nameRecipe = "Esparguete à Bolonhesa";
		recipe.setNameRecipe(nameRecipe);

		Integer numberOfRatings = 0;
		recipe.setNumberOfRatings(numberOfRatings);

		Integer prepTime = 10;
		recipe.setPrepTime(prepTime);

		Integer rating = 0;
		recipe.setRating(rating);

		List<UserCategory> userCategoryList = new ArrayList<UserCategory>();
		userCategoryList.add(new UserCategory("NORMAL"));
		recipe.setUserCategoryList(userCategoryList);
		// CRIAR RECEITA

		Recipe newRecipe = recipeService.addRecipe(recipe);
		System.out.println(" Recipe " + newRecipe);
		return new ResponseEntity<Recipe>(newRecipe, HttpStatus.OK);
	}

	@GetMapping("deleteRecipeByName/{recipeName}")
	public ResponseEntity<Boolean> deleteRecipeByName(@PathVariable("recipeName") String recipeName) {
		recipeName = "Arroz de Pato com Bacon e Laranja";
		boolean recipeDelete = recipeService.deleteRecipeByName(recipeName);
		System.out.println("Recipe delete " + recipeDelete);
		return new ResponseEntity<Boolean>(recipeDelete, HttpStatus.OK);
	}
	
	

}
