package com.recipe.app;
import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import com.recipe.app.controller.TokenFilter;



@SpringBootApplication
public class MyApplication {  
	
	@Bean
	public FilterRegistrationBean filtroJwt(){
		FilterRegistrationBean frb = new FilterRegistrationBean();
		frb.setFilter(new TokenFilter());
		frb.addUrlPatterns("/dashboard/**");
		return frb;
	}
	public static void main(String[] args) {
		
		
		SpringApplication.run(MyApplication.class, args);
    }       
}            