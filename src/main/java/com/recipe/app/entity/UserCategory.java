package com.recipe.app.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "user_category")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UserCategory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_user_category")
	private Integer idUserCategory;
	
	
	@Column(name = "name_user_category")
	private String nameUserCategory;

	@ManyToMany
	@JoinTable(name = "user_has_user_category", joinColumns = @JoinColumn(name = "id_user_category", referencedColumnName = "id_user_category"), inverseJoinColumns = @JoinColumn(name = "id_user", referencedColumnName = "id_user"))
	@JsonBackReference
	public List<User> userList = new ArrayList<User>();
	
	@ManyToMany
	@JoinTable(name = "recipe_has_user_category", joinColumns = @JoinColumn(name = "id_user_category", referencedColumnName = "id_user_category"), inverseJoinColumns = @JoinColumn(name = "id_recipe", referencedColumnName = "id_recipe"))
	@JsonBackReference
	public List<Recipe> recipeList = new ArrayList<Recipe>();

	public UserCategory() {
		super();
	}
	
	public UserCategory(String nameUserCategory) {
		// TODO Auto-generated constructor stub
		this.nameUserCategory = nameUserCategory;
	}
	
	public UserCategory(String nameUserCategory, List<User> userList, List<Recipe> recipeList) {
		super();
		this.nameUserCategory = nameUserCategory;
		this.userList = userList;
		this.recipeList = recipeList;
	}

	public Integer getIdUserCategory() {
		return idUserCategory;
	}

	public void setIdUserCategory(Integer idUserCategory) {
		this.idUserCategory = idUserCategory;
	}

	public String getNameUserCategory() {
		return nameUserCategory;
	}

	public void setNameUserCategory(String nameUserCategory) {
		this.nameUserCategory = nameUserCategory;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public List<Recipe> getRecipeList() {
		return recipeList;
	}

	public void setRecipeList(List<Recipe> recipeList) {
		this.recipeList = recipeList;
	}

	@Override
	public String toString() {
		return "UserCategory [idUserCategory=" + idUserCategory + ", nameUserCategory=" + nameUserCategory + "]";
	}

	

	
	


}
