package com.recipe.app.entity;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ingredient")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Ingredient implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_ingredient")
	private Integer idIngredient;

	@Column(name = "ingredient_name")
	private String ingredientName;

	@ManyToOne
	@JsonBackReference
	@JoinColumn(name = "category_ingredient_b_id", referencedColumnName = "id_category_ingredient_b")
	private CategoryB categoryIngredientB;

	@ManyToOne
	@JsonBackReference
	@JoinColumn(name = "measure_id", referencedColumnName = "id_measure")
	private Measure measure;

	// @ManyToMany(cascade = CascadeType.ALL)
	// @JoinTable(name = "recipe_has_ingredient", joinColumns = @JoinColumn(name
	// = "id_ingredient", referencedColumnName = "id_ingredient"),
	// inverseJoinColumns = @JoinColumn(name = "id_recipe", referencedColumnName
	// = "id_recipe"))
	// @JsonBackReference

	@OneToMany(mappedBy = "ingredient")
	@JsonManagedReference
	private List<RecipeIngredient> recipeList = new ArrayList<RecipeIngredient>();
//	public List<RecipeIngredient> recipeList = new ArrayList<RecipeIngredient>();

	



	public Ingredient() {
	}





	public Ingredient(String ingredientName, CategoryB categoryB, Measure measure2) {
		// TODO Auto-generated constructor stub
		this.ingredientName = ingredientName;
		this.categoryIngredientB = categoryB;
		this.measure = measure2;
	}





	public Integer getIdIngredient() {
		return idIngredient;
	}





	public void setIdIngredient(Integer idIngredient) {
		this.idIngredient = idIngredient;
	}





	public String getIngredientName() {
		return ingredientName;
	}





	public void setIngredientName(String ingredientName) {
		this.ingredientName = ingredientName;
	}





	public CategoryB getCategoryIngredientB() {
		return categoryIngredientB;
	}





	public void setCategoryIngredientB(CategoryB categoryIngredientB) {
		this.categoryIngredientB = categoryIngredientB;
	}





	public Measure getMeasure() {
		return measure;
	}





	public void setMeasure(Measure measure) {
		this.measure = measure;
	}





	public List<RecipeIngredient> getRecipeList() {
		return recipeList;
	}





	public void setRecipeList(List<RecipeIngredient> recipeList) {
		this.recipeList = recipeList;
	}





	@Override
	public String toString() {
		return "Ingredient [idIngredient=" + idIngredient + ", ingredientName=" + ingredientName
				+ ", categoryIngredientB=" + categoryIngredientB + ", measure=" + measure + "]";
	}


}
