package com.recipe.app.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name = "category_ingredient_a")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CategoryA implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_name_category_ingredient_a")
	private int idCategoryIngredientA;
	
	
	@Column(name = "name_category_ingredient_a")
	private String nameCategoryIngredientA;
	
	@OneToMany(mappedBy = "categoryIngredientA")
	@JsonBackReference
	private List<CategoryB> listCategoriesB = new ArrayList<CategoryB>();
//	private Set<CategoryB> listCategoriesB = new HashSet<CategoryB>();

	public CategoryA() {
	}

	public CategoryA(String nameCategoryIngredientA) {
		this.nameCategoryIngredientA = nameCategoryIngredientA;
	}

	public int getIdCategoryIngredientA() {
		return idCategoryIngredientA;
	}

	public void setIdCategoryIngredientA(int idCategoryIngredientA) {
		this.idCategoryIngredientA = idCategoryIngredientA;
	}

	public String getNameCategoryIngredientA() {
		return nameCategoryIngredientA;
	}

	public void setNameCategoryIngredientA(String nameCategoryIngredientA) {
		this.nameCategoryIngredientA = nameCategoryIngredientA;
	}


	public List<CategoryB> getListCategoriesB() {
		return listCategoriesB;
	}

	public void setListCategoriesB(List<CategoryB> listCategoriesB) {
		this.listCategoriesB = listCategoriesB;
	}

	@Override
	public String toString() {
		return "CategoryA [idCategoryIngredientA=" + idCategoryIngredientA + ", nameCategoryIngredientA="
				+ nameCategoryIngredientA + "]";
	}
 
}
