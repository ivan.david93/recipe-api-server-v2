package com.recipe.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "recipe_has_ingredient")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RecipeIngredient implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "id_recipe")
	private Recipe recipe;

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "id_ingredient")
	private Ingredient ingredient;
	
	@Column(name = "quantity")
	private int quantity;
	
	public RecipeIngredient() {
	}

	public RecipeIngredient(Recipe recipe2, Ingredient novoIngrediente, int quantity) {
		this.recipe =recipe2;
		this.ingredient = novoIngrediente;
		this.quantity = quantity;
	}

	public Recipe getRecipe() {
		return recipe;
	}

	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}

	public Ingredient getIngredient() {
		return ingredient;
	}

	public void setIngredient(Ingredient ingredient) {
		this.ingredient = ingredient;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "RecipeIngredient [recipe=" + recipe + ", ingredient=" + ingredient + ", quantity=" + quantity + "]";
	}
	
	
}
