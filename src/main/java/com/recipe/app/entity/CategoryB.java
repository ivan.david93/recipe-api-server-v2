package com.recipe.app.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "category_ingredient_b")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CategoryB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_category_ingredient_b")
	private Integer idCategoryIngredientB;

	@Column(name = "name_category_ingredient_b")
	private String nameCategoryIngredientB;

	@ManyToOne
	@JsonManagedReference
	@JoinColumn(name = "category_ingredient_a_id", referencedColumnName = "id_name_category_ingredient_a")
	private CategoryA categoryIngredientA;

	
	@OneToMany(mappedBy = "categoryIngredientB")
	@JsonManagedReference
	private List<Ingredient> ingredients = new ArrayList<Ingredient>();
//	private Set<Ingredient> ingredients = new HashSet<Ingredient>();

	public CategoryB() {
	}
	


	public CategoryB(String nameCategoryIngredientB, CategoryA categoryA) {
		this.nameCategoryIngredientB = nameCategoryIngredientB;
		this.categoryIngredientA =categoryA;
	}



	public Integer getIdCategoryIngredientB() {
		return idCategoryIngredientB;
	}


	public void setIdCategoryIngredientB(Integer idCategoryIngredientB) {
		this.idCategoryIngredientB = idCategoryIngredientB;
	}


	public String getNameCategoryIngredientB() {
		return nameCategoryIngredientB;
	}


	public void setNameCategoryIngredientB(String nameCategoryIngredientB) {
		this.nameCategoryIngredientB = nameCategoryIngredientB;
	}


	public CategoryA getCategoryIngredientA() {
		return categoryIngredientA;
	}


	public void setCategoryIngredientA(CategoryA categoryIngredientA) {
		this.categoryIngredientA = categoryIngredientA;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}



	@Override
	public String toString() {
		return "CategoryB [idCategoryIngredientB=" + idCategoryIngredientB + ", nameCategoryIngredientB="
				+ nameCategoryIngredientB + ", categoryIngredientA=" + categoryIngredientA + "]";
	}



}
