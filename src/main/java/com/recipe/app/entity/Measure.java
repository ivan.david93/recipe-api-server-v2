package com.recipe.app.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "measure")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Measure  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_measure")
	private int idMeasure;
	
	@Column(name = "name_measure")
	private String nameMeasure;
	
	@Column(name = "initials_measure")
	private String initialsMeasure;
	
	@OneToMany(mappedBy = "measure")
	@JsonManagedReference
	private List<Ingredient> ingredients = new ArrayList<Ingredient>();

	
	
	public Measure() {
	}

	
	public Measure(int idMeasure ,String nameMeasure, String initialsMeasure) {
		this.idMeasure = idMeasure;
		this.nameMeasure = nameMeasure;
		this.initialsMeasure = initialsMeasure;
	}

	public String getNameMeasure() {
		return nameMeasure;
	}

	public void setNameMeasure(String nameMeasure) {
		this.nameMeasure = nameMeasure;
	}

	public String getInitialsMeasure() {
		return initialsMeasure;
	}

	public void setInitialsMeasure(String initialsMeasure) {
		this.initialsMeasure = initialsMeasure;
	}


	public int getIdMeasure() {
		return idMeasure;
	}


	public void setIdMeasure(int idMeasure) {
		this.idMeasure = idMeasure;
	}


	public List<Ingredient> getIngredients() {
		return ingredients;
	}


	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}


	@Override
	public String toString() {
		return "Measure [idMeasure=" + idMeasure + ", nameMeasure=" + nameMeasure + ", initialsMeasure="
				+ initialsMeasure + "]";
	}

	
}
