package com.recipe.app.entity;

import java.io.Serializable;

public class IngredientHelper implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer idIngredient;
	
	private String ingredientName;
	
	private Measure measure;
	
	private int quantity;
	
	
	

	

	public IngredientHelper(Integer idIngredient, String ingredientName, Measure measure, int quantity) {
		super();
		this.idIngredient = idIngredient;
		this.ingredientName = ingredientName;
		this.measure = measure;
		this.quantity = quantity;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Integer getIdIngredient() {
		return idIngredient;
	}

	public void setIdIngredient(Integer idIngredient) {
		this.idIngredient = idIngredient;
	}

	public String getIngredientName() {
		return ingredientName;
	}

	public void setIngredientName(String ingredientName) {
		this.ingredientName = ingredientName;
	}

	public Measure getMeasure() {
		return measure;
	}

	public void setMeasure(Measure measure) {
		this.measure = measure;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

		

}
