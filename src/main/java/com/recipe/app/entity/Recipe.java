package com.recipe.app.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "recipe")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Recipe implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_recipe")
	private Integer idRecipe;

	@Column(name = "name_recipe")
	private String nameRecipe;

	@Column(name = "media")
	private String media;

	@Column(name = "rating")
	private Integer rating;

	@Column(name = "difficulty")
	private Integer difficulty;

	@Column(name = "prep_time")
	private Integer prepTime;

	@Column(name = "create_date")
	@Type(type="date")
	private Date createDate;

	@Column(name = "update_date")
	@Type(type="date")
	private Date updateDate;

	@Column(name = "number_of_ratings")
	private Integer numberOfRatings;

	@Column(name = "directions")
	private String directions;

	@OneToMany(mappedBy = "recipe")
	@JsonBackReference
	public List<RecipeIngredient> ingredientList = new ArrayList<RecipeIngredient>();

	@ManyToMany
	@JoinTable(name = "recipe_has_user_category", joinColumns = @JoinColumn(name = "id_recipe", referencedColumnName = "id_recipe"), inverseJoinColumns = @JoinColumn(name = "id_user_category", referencedColumnName = "id_user_category"))
	@JsonBackReference
	public List<UserCategory> userCategoryList = new ArrayList<UserCategory>();

	public Recipe() {
		super();
	}

	public Recipe(String nameRecipe, String media, Integer rating, Integer difficulty, Integer prepTime,
			Date createDate, Date updateDate, Integer numberOfRatings, String directions,
			List<RecipeIngredient> ingredientList, List<UserCategory> userCatagoryList) {
		super();
		this.nameRecipe = nameRecipe;
		this.media = media;
		this.rating = rating;
		this.difficulty = difficulty;
		this.prepTime = prepTime;
		this.createDate = createDate;
		this.updateDate = updateDate;
		this.numberOfRatings = numberOfRatings;
		this.directions = directions;
		this.ingredientList = ingredientList;
		this.userCategoryList = userCatagoryList;
	}

	public Recipe(String nameRecipe, String media, Integer rating, Integer difficulty, Integer prepTime,
			Date createDate, Date updateDate, Integer numberOfRatings, String directions,
			List<RecipeIngredient> ingredientList) {
		super();
		this.nameRecipe = nameRecipe;
		this.media = media;
		this.rating = rating;
		this.difficulty = difficulty;
		this.prepTime = prepTime;
		this.createDate = createDate;
		this.updateDate = updateDate;
		this.numberOfRatings = numberOfRatings;
		this.directions = directions;
		this.ingredientList = ingredientList;
	}

	public Integer getIdRecipe() {
		return idRecipe;
	}

	public void setIdRecipe(Integer idRecipe) {
		this.idRecipe = idRecipe;
	}

	public String getNameRecipe() {
		return nameRecipe;
	}

	public void setNameRecipe(String nameRecipe) {
		this.nameRecipe = nameRecipe;
	}

	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Integer getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(Integer difficulty) {
		this.difficulty = difficulty;
	}

	public Integer getPrepTime() {
		return prepTime;
	}

	public void setPrepTime(Integer prepTime) {
		this.prepTime = prepTime;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Integer getNumberOfRatings() {
		return numberOfRatings;
	}

	public void setNumberOfRatings(Integer numberOfRatings) {
		this.numberOfRatings = numberOfRatings;
	}

	public String getDirections() {
		return directions;
	}

	public void setDirections(String directions) {
		this.directions = directions;
	}

	public List<RecipeIngredient> getIngredientList() {
		return ingredientList;
	}

	public void setIngredientList(List<RecipeIngredient> ingredientList) {
		this.ingredientList = ingredientList;
	}

	public List<UserCategory> getUserCategoryList() {
		return userCategoryList;
	}

	public void setUserCategoryList(List<UserCategory> userCategoryList) {
		this.userCategoryList = userCategoryList;
	}

	@Override
	public String toString() {
		return "Recipe [idRecipe=" + idRecipe + ", nameRecipe=" + nameRecipe + ", media=" + media + ", rating=" + rating
				+ ", difficulty=" + difficulty + ", prepTime=" + prepTime + ", createDate=" + createDate
				+ ", updateDate=" + updateDate + ", numberOfRatings=" + numberOfRatings + ", directions=" + directions
				+ "]";
	}

	

}
