package com.recipe.app.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "user")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_user")
	private Integer idUser;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "user_has_user_category", joinColumns = @JoinColumn(name = "id_user", referencedColumnName = "id_user"), inverseJoinColumns = @JoinColumn(name = "id_user_category", referencedColumnName = "id_user_category"))
	@JsonBackReference
	public List<UserCategory> userCategoryList = new ArrayList<UserCategory>();

	public User() {
		super();
	}

	public User(Integer idUser, List<UserCategory> userCategoryList) {
		super();
		this.idUser = idUser;
		this.userCategoryList = userCategoryList;
	}

	public List<UserCategory> getUserCategoryList() {
		return userCategoryList;
	}

	public void setUserCategoryList(List<UserCategory> userCategoryList) {
		this.userCategoryList = userCategoryList;
	}
	
	

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	@Override
	public String toString() {
		return "User [idUser=" + idUser + "]";
	}

	

}
