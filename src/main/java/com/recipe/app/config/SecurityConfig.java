package com.recipe.app.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private static final String USER = "USER";

	private static final String ADMIN = "ADMIN";

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.authorizeRequests().antMatchers("/dashboard/login").permitAll();
		//.and().formLogin().loginPage("/dashboard/login").permitAll();//
		//.hasRole(ADMIN).anyRequest().authenticated().and()
				//.formLogin();
				//.loginPage("/dashboard");
		// .permitAll()
		// .and()
		// .logout()
		// .permitAll();
		httpSecurity.csrf().disable();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
		.withUser("user").password("password").roles(USER)
		.and()
		.withUser("root").password("root").roles(ADMIN);
	}

}
