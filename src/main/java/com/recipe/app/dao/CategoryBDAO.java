package com.recipe.app.dao;

import java.util.List;

import com.recipe.app.entity.CategoryA;
import com.recipe.app.entity.CategoryB;

public interface CategoryBDao {
	
	//_____________________________________________________________________________________________________________
	//___________________________________  CategoryB  _____________________________________________________________
	//_____________________________________________________________________________________________________________
	
	List<CategoryB> findAllCategoriesB();
	
	CategoryB findCategoryBById(int idCategoryB);
	
	CategoryB findCategoryBByName(String categoryB);
	
	CategoryB addCategoryB(CategoryB newCategoryB);

	CategoryB updateCategoryB(CategoryB newCategoryB, CategoryA newCategA);

	boolean deleteCategoryB(int idCategoryB);


	

	

}
