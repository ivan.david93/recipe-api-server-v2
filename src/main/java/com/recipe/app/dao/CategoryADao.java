package com.recipe.app.dao;

import java.util.List;

import com.recipe.app.entity.CategoryA;

public interface CategoryADao {

	//_____________________________________________________________________________________________________________
	//___________________________________  CategoryA  _____________________________________________________________
	//_____________________________________________________________________________________________________________
	
	List<CategoryA> findAllCategoriesA();
	
	CategoryA findCategoryAById(int idCategoryA);

	CategoryA findCategoryAByName(String nameCategoryA);	

	CategoryA updateCategoryA(int idCategoryA, CategoryA newCategoryA);

	CategoryA addCategoryA(CategoryA newCategoryA);

	boolean deleteCategoryA(int idCategoryA);
	

}
