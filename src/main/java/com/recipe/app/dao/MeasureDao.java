package com.recipe.app.dao;

import java.util.List;

import com.recipe.app.entity.CategoryB;
import com.recipe.app.entity.Measure;

public interface MeasureDao {

	List<Measure> findAllMeasures();

	Measure findMeasureById(int idMeasure);

	Measure findMeasureByName(String nameMeasure);

	Measure updateMeasure(Measure newMeasure);

	Measure addMeasure(Measure newMeasure);

	boolean deleteMeasure(int idMeasure);

}
