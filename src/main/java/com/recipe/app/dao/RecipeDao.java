package com.recipe.app.dao;

import java.util.List;

import com.recipe.app.entity.Recipe;

public interface RecipeDao {
	
	List<Recipe> getAllRecipes();

	Recipe addRecipe(Recipe recipe);

	boolean deleteRecipeByName(String name);

	Recipe findRecipeByName(String recipeName);

	Recipe findRecipeById(int idRecipe);

	Recipe updateRecipe(Recipe recipeOld);

}
