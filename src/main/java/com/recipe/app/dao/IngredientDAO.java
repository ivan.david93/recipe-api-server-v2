package com.recipe.app.dao;

import java.util.List;

import com.recipe.app.entity.Ingredient;

public interface IngredientDao {

	List<Ingredient> findAllIngredients();

	Ingredient findIngredientById(Integer ingredientId);
	
	Ingredient findIngredientByName(String idIngredient);

	Ingredient addIngredient(Ingredient ingredient);

	Ingredient updateIngredient(Ingredient ingredient);

	boolean deleteIngredient(int idIngredient);

	Ingredient findRecipesByName(String idIngredient);



}
