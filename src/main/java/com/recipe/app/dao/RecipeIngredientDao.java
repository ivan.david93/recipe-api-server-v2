package com.recipe.app.dao;

import java.util.List;

import com.recipe.app.entity.Recipe;
import com.recipe.app.entity.RecipeIngredient;

public interface RecipeIngredientDao {

	List<RecipeIngredient> findIngredientsByRecipe(Recipe recipeDB);
	

}
