package com.recipe.app.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class AbstractJpaDao<T extends Serializable> {

	private Class<T> clazz;

	@PersistenceContext
	EntityManager entityManager;

	public void setClazz(Class<T> clazzToSet) {
		this.clazz = clazzToSet;
	}

	public T findOne(int id) {
		return entityManager.find(clazz, id);
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		return entityManager.createQuery("from " + clazz.getName()).getResultList();
	}

	public void save(T entity) {
		entityManager.persist(entity);
	}

	public  T update(T entity) {
		T entityUpd = entityManager.merge(entity);
		return entityUpd;
	}

	public void delete(T entity) {
		entityManager.remove(entity);
		
	}

	public void deleteById(int entityId) {
		T entity = findOne(entityId);
		delete(entity);
	}
}