package com.recipe.app.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.recipe.app.dao.RecipeDao;
import com.recipe.app.entity.CategoryA;
import com.recipe.app.entity.Ingredient;
import com.recipe.app.entity.Recipe;

@Transactional
@Repository("recipedao")
public class RecipeDaoImpl extends AbstractJpaDao<Recipe> implements RecipeDao {

	@Override
	public List<Recipe> getAllRecipes() {

		setClazz(Recipe.class);

		List<Recipe> listRecipe = findAll();
		return listRecipe;
		// String hql = "FROM Article as atcl ORDER BY atcl.articleId";
		// return (List<Article>)
		// entityManager.createQuery(hql).getResultList();
	}

	@Override
	public Recipe addRecipe(Recipe recipe) {
		String nameRecipe = recipe.getNameRecipe();

		setClazz(Recipe.class);

		List<Recipe> listRec = findByName(nameRecipe);

		if (listRec.isEmpty()) {
			save(recipe);
		}

		Recipe recipe1 = (Recipe) findByName(nameRecipe).get(0);

		return recipe1;
	}

	private List<Recipe> findByName(String nameRecipe) {
		// nameRecipe = "Arroz de Pato com Bacon e Laranja";

		List<Recipe> recipeL = null;
		String hql = "FROM Recipe as rec WHERE rec.nameRecipe = ? ";
		try {
			recipeL = entityManager.createQuery(hql).setParameter(1, nameRecipe).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}

		return recipeL;
	}

	@Override
	public boolean deleteRecipeByName(String name) {
		List<Recipe> listRec = findByName(name);
		if (listRec.isEmpty()) {
			return false;
		}
		Recipe toDeleteRecipe = listRec.get(0);

		delete(toDeleteRecipe);
		List<Recipe> recipeListNew = findByName(name);
		if (recipeListNew.isEmpty()) { // se lista depois de apagar o nome
										// estiver vazia quer dizer q apagou
										// return true
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Recipe findRecipeByName(String recipeName) {

		List<Recipe> listRec = findByName(recipeName);
		if (listRec.isEmpty()) {
			return null;
		} else {
			return listRec.get(0);
		}
	}

	@Override
	public Recipe findRecipeById(int idRecipe) {
		// TODO Auto-generated method stub
		Recipe recipe = null;
		try {
			setClazz(Recipe.class);
			recipe = findOne(idRecipe);
		} catch (Exception e) {
			throw e;
		}

		return recipe;
	}

	@Override
	public Recipe updateRecipe(Recipe newRecipe) {

		Recipe newRecipeDB = null;
		try {
			setClazz(Recipe.class);
			newRecipeDB = update(newRecipe);

		} catch (

		Exception e) {
			System.err.println("______________________Erro no método updateCategoryB____________________");
			throw e;
		}
		return newRecipeDB;
	}

}
