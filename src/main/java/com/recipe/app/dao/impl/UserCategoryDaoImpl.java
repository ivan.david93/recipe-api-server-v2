package com.recipe.app.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.recipe.app.dao.UserCategoryDao;
import com.recipe.app.entity.Ingredient;
import com.recipe.app.entity.Recipe;
import com.recipe.app.entity.User;
import com.recipe.app.entity.UserCategory;

@Transactional
@Repository("usercategorydao")
public class UserCategoryDaoImpl extends AbstractJpaDao<UserCategory> implements UserCategoryDao {

	@Override
	public List<UserCategory> findAllUserCategory() {
		setClazz(UserCategory.class);
		List<UserCategory> listUser = findAll();
		return listUser;
	}

	@Override
	public boolean deleteUserCategoryByName(String nameCategory) {

		setClazz(UserCategory.class);

		UserCategory userCategory = findUserCategoryByName(nameCategory);
		if (userCategory == null) {
			return false;
		}

		delete(userCategory);
		UserCategory userCategoryNew = findUserCategoryByName(nameCategory);
		if (userCategoryNew == null) { // se lista depois de apagar o nome
										// estiver vazia quer dizer q apagou
										// return true
			return true;
		} else {
			return false;
		}
	}

	@Override
	public UserCategory findUserCategoryByName(String nameCategory) {

		String hql = "FROM UserCategory as uc WHERE uc.nameUserCategory = ? ";
		List<UserCategory> userCategoryList = entityManager.createQuery(hql).setParameter(1, nameCategory)
				.getResultList();

		if (userCategoryList.isEmpty()) {
			return null;
		}
		UserCategory userCategory = userCategoryList.get(0);
		return userCategory;
	}

	@Override
	public UserCategory updateUserCategory(UserCategory userCatdb) {
		// TODO Auto-generated method stub
		setClazz(UserCategory.class);
		UserCategory userUpdt = update(userCatdb);

		return userUpdt;
	}

	@Override
	public boolean addUserCategory(UserCategory newUserCategory) {
		setClazz(UserCategory.class);
		save(newUserCategory);

		UserCategory userCatNew = findUserCategoryByName(newUserCategory.getNameUserCategory());
		if (userCatNew != null) {
			return true;
		}
		return false;
	}

	@Override
	public UserCategory findUserCategoryById(Integer idUserCategory) {
		return entityManager.find(UserCategory.class, idUserCategory);
	}

	@Override
	public List<UserCategory> findUserCategoryByUserId(int userId) {
		
		
		return null;
	}

}
