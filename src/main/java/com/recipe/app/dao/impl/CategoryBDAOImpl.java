package com.recipe.app.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.recipe.app.dao.CategoryBDao;
import com.recipe.app.entity.CategoryA;
import com.recipe.app.entity.CategoryB;
import com.recipe.app.entity.Ingredient;


@Transactional
@Repository("categorybdao")
public class CategoryBDaoImpl extends AbstractJpaDao<CategoryB> implements CategoryBDao{
	
	//_____________________________________________________________________________________________________________
	//___________________________________  CategoryB  _____________________________________________________________
	//_____________________________________________________________________________________________________________
	
	@Override
	public 	List<CategoryB> findAllCategoriesB() {
		setClazz(CategoryB.class);
		List<CategoryB> listCategoriesB = findAll();
		return listCategoriesB;
	}
	
	@Override
	public CategoryB findCategoryBById(int idCategoryB) {
		CategoryB categoryB = null;
		try {
			setClazz(CategoryB.class);
			categoryB = findOne(idCategoryB);
		} catch (Exception e) {
			System.err.println("______________________Erro no método findCategoryBById________________________");
		}

		return categoryB;
	}
	
	@Override
	public CategoryB findCategoryBByName(String nameCategoryB) {
		
//		String queryCatB = "from CategoryB left outer join CategoryA on CategoryB.categoryIngredientA=CategoryA.nameCategoryIngredientA where CategoryB.nameCategoryIngredientB=?";
////		String queryCatB = "FROM CategoryB as catB where catB.nameCategoryIngredientB = ? and catB.categoryIngredientA = ?";
//		List<CategoryB> categoryB_db = entityManager.createQuery(queryCatB).setParameter(1, categoryB).getResultList();
//		
//		return categoryB_db.get(0);
		
		String queryGetCategoryB = "from CategoryB as cat_b where cat_b.nameCategoryIngredientB=?";
		List<CategoryB> categoryB_db = entityManager.createQuery(queryGetCategoryB).setParameter(1, nameCategoryB)
				.getResultList();

		return categoryB_db.isEmpty() ? null : categoryB_db.get(0);
	}

	@Override
	public CategoryB addCategoryB(CategoryB newCategoryB) {
		String nameCategB = newCategoryB.getNameCategoryIngredientB();

		CategoryB checkCategoryB = findCategoryBByName(nameCategB);
		
		if (checkCategoryB == null) {
			setClazz(CategoryB.class);
			save(newCategoryB);

			CategoryB checkAddCategoryB = findCategoryBByName(nameCategB);
			if (checkAddCategoryB != null) {
				return checkAddCategoryB;
			}
		}

		return null;
	}
	
	@Override
	public CategoryB updateCategoryB(CategoryB newCategoryB, CategoryA newCategA) {
		
		CategoryB originalCategB = findCategoryBById(newCategoryB.getIdCategoryIngredientB());
		
	
		
		originalCategB.setCategoryIngredientA(newCategA);
		originalCategB.setNameCategoryIngredientB(newCategoryB.getNameCategoryIngredientB());
		
		CategoryB alteredCategoryB = null;
		try {
			setClazz(CategoryB.class);
			alteredCategoryB = update(originalCategB);
		} catch (Exception e) {
			System.err.println("______________________Erro no método updateCategoryB____________________");
		}

		return alteredCategoryB;
		
	}

	@Override
	public boolean deleteCategoryB(int idCategoryB) {
		setClazz(CategoryB.class);
		deleteById(idCategoryB);
		
		CategoryB categoryB = findCategoryBById(idCategoryB);
		
		if(categoryB == null){
			return true;
		}
		return false;
	}
	

}

