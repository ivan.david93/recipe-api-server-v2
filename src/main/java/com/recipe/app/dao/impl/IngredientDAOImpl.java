package com.recipe.app.dao.impl;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.recipe.app.dao.IngredientDao;
import com.recipe.app.entity.CategoryA;
import com.recipe.app.entity.CategoryB;
import com.recipe.app.entity.Ingredient;
import com.recipe.app.entity.Recipe;
import com.recipe.app.entity.RecipeIngredient;

@Transactional
@Repository("ingredientdao")
public class IngredientDaoImpl extends AbstractJpaDao<Ingredient> implements IngredientDao {

	@Override
	public List<Ingredient> findAllIngredients() {
		setClazz(Ingredient.class);
		List<Ingredient> listIngredient = findAll();
		return listIngredient;
	}

	@Override
	public Ingredient findIngredientById(Integer ingredientId) {
		return entityManager.find(Ingredient.class, ingredientId);
	}

	@Override
	public Ingredient findIngredientByName(String ingredient) {

		String queryGetIngredient = "from Ingredient as ing where ing.ingredientName=?";
		List<Ingredient> ingredient_db = null;
		ingredient_db = entityManager.createQuery(queryGetIngredient).setParameter(1, ingredient).getResultList();

		return ingredient_db.isEmpty() ? null : ingredient_db.get(0);
	}

	@Override
	public Ingredient findRecipesByName(String idIngredient) {

		String queryGetIngredient = "from Ingredient as ing where ing.idIngredient=?";
		List<Ingredient> ingredient_db = entityManager.createQuery(queryGetIngredient).setParameter(1, idIngredient)
				.getResultList();

		return ingredient_db.get(0);
	}

	@Override
	public Ingredient addIngredient(Ingredient newIngredient) {

		String nameIngredient = newIngredient.getIngredientName();

		Ingredient checkIngredient = findIngredientByName(nameIngredient);

		if (checkIngredient == null) {
			setClazz(Ingredient.class);
			save(newIngredient);

			Ingredient checkAddIngredient = findIngredientByName(nameIngredient);
			if (checkAddIngredient != null) {
				return checkAddIngredient;
			}
		}

		return null;
	}

	@Override
	public Ingredient updateIngredient(Ingredient newIngredient) {
		// Ingredient getIngredient =
		// findIngredientById(ingredient.getIdIngredient());
		// //
		// getIngredient.setCategorieIngredientB(ingredient.getCategorieIngredientB());
		// getIngredient.setIngredientName(ingredient.getIngredientName());
		// getIngredient.setMeasure(ingredient.getMeasure());
		// getIngredient.setRecipeList(ingredient.getRecipeList());
		// entityManager.flush();
		// }
		Ingredient originalIngredient = findIngredientById(newIngredient.getIdIngredient());

		originalIngredient.setIngredientName(newIngredient.getIngredientName());
		originalIngredient.setMeasure(newIngredient.getMeasure());
		originalIngredient.setCategoryIngredientB(newIngredient.getCategoryIngredientB());

		Ingredient alteredIngredient = null;

		try {
			setClazz(Ingredient.class);
			alteredIngredient = update(originalIngredient);

		} catch (

		Exception e) {
			System.err.println("______________________Erro no método updateCategoryB____________________");
		}
		return alteredIngredient;

	}

	// CategoryB originalCategB =
	// findCategoryBById(newCategoryB.getIdCategoryIngredientB());
	//
	//
	//
	// originalCategB.setCategoryIngredientA(newCategA);
	// originalCategB.setNameCategoryIngredientB(newCategoryB.getNameCategoryIngredientB());
	//
	// CategoryB alteredCategoryB = null;
	// try {
	// setClazz(CategoryB.class);
	// alteredCategoryB = update(originalCategB);
	// } catch (Exception e) {
	// System.err.println("______________________Erro no método
	// updateCategoryB____________________");
	// }
	//
	// return alteredCategoryB;

	@Override
	public boolean deleteIngredient(int idIngredient) {

		setClazz(Ingredient.class);
		deleteById(idIngredient);

		Ingredient ingredient = findIngredientById(idIngredient);

		if (ingredient == null) {
			return true;
		}
		return false;
	}

}
