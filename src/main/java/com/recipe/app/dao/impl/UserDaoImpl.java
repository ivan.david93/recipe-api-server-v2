package com.recipe.app.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.recipe.app.dao.UserDao;
import com.recipe.app.entity.CategoryB;
import com.recipe.app.entity.Recipe;
import com.recipe.app.entity.User;
import com.recipe.app.entity.UserCategory;

@Transactional
@Repository("userdao")
public class UserDaoImpl  extends AbstractJpaDao<User> implements UserDao{

	@Override
	public List<User> findAllUsers() {
		setClazz(User.class);
		List<User> listUser = findAll();
		return listUser;
	}

	@Override
	public boolean deleteUserById(int id) {
		setClazz(User.class);
		User user = findOne(id);
		if (user == null) {
			return false;
		}

		delete(user);
		User userNew = findOne(id);
		if (userNew == null) { // se lista depois de apagar o nome estiver vazia quer dizer q apagou return true
			return true;
		}
		else{
			return false;
		}
	}

	@Override
	public User addUser(User newUser) {
		setClazz(User.class);
		save(newUser);

		User userNew = findOne(newUser.getIdUser());
		if (userNew != null) {
			return userNew;
		}
		return null;
	}

	@Override
	public User findUserById(int idUser) {
		setClazz(User.class);
		User userdb = findOne(idUser);
		if (userdb != null) {
			return userdb;
		}
		return null;
	}

	@Override
	public User updateUser(User userdb) {
		
		setClazz(User.class);
		
		update(userdb);
		
		User userUpdated = findOne(userdb.getIdUser());
		if (userUpdated != null) {
			return userUpdated;
		}
		return null;
	}

	@Override
	public Integer getNewId() {
		// TODO Auto-generated method stub
		
		String getLastId = "SELECT MAX(idUser) as idUser from User";
		
		List lastId = entityManager.createQuery(getLastId).getResultList();
		if (!lastId.isEmpty()) {
			Integer id = (Integer) lastId.get(0) + 1;
			return id;
		}
		
		
		return null;
	}
	

}
