package com.recipe.app.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.recipe.app.dao.MeasureDao;
import com.recipe.app.entity.CategoryA;
import com.recipe.app.entity.CategoryB;
import com.recipe.app.entity.Measure;

@Transactional
@Repository("measuredao")
public class MeasureDaoImpl extends AbstractJpaDao<Measure> implements MeasureDao{
	
	//_____________________________________________________________________________________________________________
	//___________________________________  Measure  _____________________________________________________________
	//_____________________________________________________________________________________________________________

	@Override
	public 	List<Measure> findAllMeasures() {
		setClazz(Measure.class);
		List<Measure> listCategoriesB = findAll();
		return listCategoriesB;
	}
	
	@Override
	public Measure findMeasureById(int idMeasure) {
		Measure measure = null;
		try {
			setClazz(Measure.class);
			measure = findOne(idMeasure);
		} catch (Exception e) {
			System.err.println("______________________Erro no método findMeasureById________________________");
		}

		return measure;
	}
	
	@Override
	public Measure findMeasureByName(String nameMeasure) {
		
//		String queryCatB = "from CategoryB left outer join CategoryA on CategoryB.categoryIngredientA=CategoryA.nameCategoryIngredientA where CategoryB.nameCategoryIngredientB=?";
////		String queryCatB = "FROM CategoryB as catB where catB.nameCategoryIngredientB = ? and catB.categoryIngredientA = ?";
//		List<CategoryB> categoryB_db = entityManager.createQuery(queryCatB).setParameter(1, categoryB).getResultList();
//		
//		return categoryB_db.get(0);
		
		String queryGetMeasure = "from Measure as measure where measure.nameMeasure=?";
		List<Measure> measure_db = entityManager.createQuery(queryGetMeasure).setParameter(1, nameMeasure)
				.getResultList();

		return measure_db.isEmpty() ? null : measure_db.get(0);
	}

	@Override
	public Measure addMeasure(Measure newMeasure) {
		String nameMeasure = newMeasure.getNameMeasure();

		Measure checkMeasure = findMeasureByName(nameMeasure);
		
		if (checkMeasure == null) {
			setClazz(Measure.class);
			save(newMeasure);

			Measure checkAddMeasure = findMeasureByName(nameMeasure);
			if (checkAddMeasure != null) {
				return checkAddMeasure;
			}
		}

		return null;
	}
	
	@Override
	public Measure updateMeasure(Measure newMeasure) {
		
		Measure originalMeasure = findMeasureById(newMeasure.getIdMeasure());
		
		originalMeasure.setNameMeasure(newMeasure.getNameMeasure());
		originalMeasure.setInitialsMeasure(newMeasure.getInitialsMeasure());
		
		Measure alteredMeasure = null;
		try {
			setClazz(Measure.class);
			alteredMeasure = update(originalMeasure);
		} catch (Exception e) {
			System.err.println("______________________Erro no método updateMeasure____________________");
		}

		return alteredMeasure;
		
	}

	@Override
	public boolean deleteMeasure(int idMeasure) {
		setClazz(Measure.class);
		deleteById(idMeasure);
		
		Measure measure = findMeasureById(idMeasure);
		
		if(measure == null){
			return true;
		}
		return false;
	}


}
