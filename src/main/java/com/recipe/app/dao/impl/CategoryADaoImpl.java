package com.recipe.app.dao.impl;

import java.util.List;

import org.hibernate.type.TrueFalseType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.recipe.app.dao.CategoryADao;
import com.recipe.app.entity.CategoryA;
import com.recipe.app.entity.Ingredient;

@Transactional
@Repository("categoryadao")
public class CategoryADaoImpl extends AbstractJpaDao<CategoryA> implements CategoryADao {

	// _____________________________________________________________________________________________________________
	// ___________________________________ CategoryA
	// _____________________________________________________________
	// _____________________________________________________________________________________________________________

	@Override
	public List<CategoryA> findAllCategoriesA() {
		setClazz(CategoryA.class);
		List<CategoryA> listCategoriesA = findAll();
		return listCategoriesA;
	}

	@Override
	public CategoryA findCategoryAById(int idCategoryA) {
		CategoryA categoryA = null;
		try {
			setClazz(CategoryA.class);
			categoryA = findOne(idCategoryA);
		} catch (Exception e) {
			System.err.println("______________________Erro no método findCategoryAById________________________");
		}

		return categoryA;
	}

	@Override
	public CategoryA findCategoryAByName(String nameCategoryA) {

		String queryGetCategoryA = "from CategoryA as cat_a where cat_a.nameCategoryIngredientA=?";
		List<CategoryA> categoryA_db = entityManager.createQuery(queryGetCategoryA).setParameter(1, nameCategoryA)
				.getResultList();

		return categoryA_db.isEmpty() ? null : categoryA_db.get(0);
	}

	@Override
	public CategoryA addCategoryA(CategoryA newCategoryA) {

		String nameCategA = newCategoryA.getNameCategoryIngredientA();

		CategoryA checkCategoryA = findCategoryAByName(nameCategA);
		
		if (checkCategoryA == null) {
			setClazz(CategoryA.class);
			save(newCategoryA);

			CategoryA checkAddCategoryA = findCategoryAByName(nameCategA);
			if (checkAddCategoryA != null) {
				return checkAddCategoryA;
			}
		}

		return null;
	}
	
	@Override
	public CategoryA updateCategoryA(int idCategoryA, CategoryA newCategoryA) {
		CategoryA categoriaA = findCategoryAById(idCategoryA);

		categoriaA.setNameCategoryIngredientA(newCategoryA.getNameCategoryIngredientA());
		CategoryA alteredCategoryA = null;
		try {
			setClazz(CategoryA.class);
			alteredCategoryA = update(categoriaA);
		} catch (Exception e) {
			System.err.println("______________________Erro no método updateCategoryA____________________");
		}

		return alteredCategoryA;

	}


	@Override
	public boolean deleteCategoryA(int idCategoryA) {
		setClazz(CategoryA.class);
		deleteById(idCategoryA);
		
		CategoryA categoriaA = findCategoryAById(idCategoryA);
		
		if(categoriaA == null){
			return true;
		}
		return false;
		
		
	}

}
