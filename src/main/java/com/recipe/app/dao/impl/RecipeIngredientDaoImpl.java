package com.recipe.app.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.recipe.app.dao.RecipeIngredientDao;
import com.recipe.app.dao.UserCategoryDao;
import com.recipe.app.entity.Recipe;
import com.recipe.app.entity.RecipeIngredient;
import com.recipe.app.entity.UserCategory;

@Transactional
@Repository("recipeingredientdao")
public class RecipeIngredientDaoImpl extends AbstractJpaDao<RecipeIngredient> implements RecipeIngredientDao {

	@Override
	public List<RecipeIngredient> findIngredientsByRecipe(Recipe recipeDB) {

		List<RecipeIngredient> recipeIngredients = null;
		String hql = "FROM RecipeIngredient as rec WHERE rec.recipe = ? ";
		try {
			recipeIngredients = entityManager.createQuery(hql).setParameter(1, recipeDB).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
		return recipeIngredients;
	}

}
