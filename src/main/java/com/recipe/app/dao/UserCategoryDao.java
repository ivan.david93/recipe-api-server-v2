package com.recipe.app.dao;

import java.util.List;

import com.recipe.app.entity.UserCategory;

public interface UserCategoryDao {

	List<UserCategory> findAllUserCategory();

	boolean deleteUserCategoryByName(String name);

	UserCategory findUserCategoryByName(String nameCategory);

	UserCategory updateUserCategory(UserCategory userCatdb);

	boolean addUserCategory(UserCategory newUserCategory);

	UserCategory findUserCategoryById(Integer idUserCategory);

	List<UserCategory> findUserCategoryByUserId(int userId);

}
