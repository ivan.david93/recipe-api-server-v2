package com.recipe.app.dao;

import java.util.List;

import com.recipe.app.entity.User;

public interface UserDao {

	List<User> findAllUsers();

	boolean deleteUserById(int id);

	User addUser(User newUser);

	User findUserById(int idUser);

	User updateUser(User userdb);

	Integer getNewId();

}
