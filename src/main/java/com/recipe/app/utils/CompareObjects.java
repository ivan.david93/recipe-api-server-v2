package com.recipe.app.utils;

import java.util.List;

import com.recipe.app.entity.IngredientHelper;
import com.recipe.app.entity.Recipe;

public class CompareObjects {

	Object obj1;
	Object obj2;

	

	public boolean compareObjects(int idObj1, int idObj2) {
		if (idObj1 == idObj2) {
			return true;
		}
		return false;
	}

	public static boolean containsRecipeWithId(List<Recipe> list, long id) {
		for (Recipe object : list) {
			if (object.getIdRecipe() == id) {
				return true;
			}
		}
		return false;
	}
	
	public static int containsIngredientHelperWithId(List<IngredientHelper> list, long id) {
		
		for (IngredientHelper object : list) {
			if (object.getIdIngredient() == id) {
				return list.indexOf(object);	
			}
		}
		//if ingredient doesnt exist return -1
		return -1;
	
	}

}
