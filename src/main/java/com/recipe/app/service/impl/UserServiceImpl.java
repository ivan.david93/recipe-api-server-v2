package com.recipe.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.recipe.app.dao.RecipeDao;
import com.recipe.app.dao.UserCategoryDao;
import com.recipe.app.dao.UserDao;
import com.recipe.app.entity.User;
import com.recipe.app.entity.UserCategory;
import com.recipe.app.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private UserCategoryDao userCategoryDao;

	@Override
	public List<User> findAllUsers() {
		// TODO Auto-generated method stub
		List<User> userList = userDao.findAllUsers();

		return userList;
	}

	@Override
	public boolean deleteUserById(int id) {
		boolean delete = userDao.deleteUserById(id); // TODO Auto-generated
														// method stub
		return delete;
	}

	@Override
	public User addUser(int id, List<UserCategory> userCategoryList) {
		// TODO Auto-generated method stub
		User newUser = new User(id, userCategoryList);

		User userCreated = userDao.addUser(newUser);

		if (userCreated != null) {
			return userCreated;
		}
		return null;

	}

	@Override
	public User findUserById(int idUser) {
		User userdb = userDao.findUserById(idUser);

		return userdb;
	}

	@Override
	public User updateUser(int idUser, List<Integer> newUserCategoryIdList) {
		// TODO Auto-generated method stub
		User userdb = userDao.findUserById(idUser);
		if (userdb == null) {
			System.out.println("User nao existe na bd id: " + idUser);
			return null;
		}
		List<UserCategory> listOfUserCategToUpdateUser = new ArrayList<>();
		for (int i = 0; i < newUserCategoryIdList.size(); i++) {
			UserCategory getUserCatById = userCategoryDao.findUserCategoryById(newUserCategoryIdList.get(i));
			listOfUserCategToUpdateUser.add(getUserCatById);
		}
		userdb.setUserCategoryList(listOfUserCategToUpdateUser); // user from db update
															// da lista de
															// usercategory
		User updateUSer = userDao.updateUser(userdb);
		if (updateUSer != null) {
			System.out.println("User update na bd id: " + updateUSer);
			return updateUSer;
		}
		return null;

	}

	@Override
	public User addUser(List<Integer> userCategoryList) {
		
		//ir buscar novo id para inserir na bd
		Integer id = userDao.getNewId();
		
		//para ir buscar a lista de user category dando a lista de ids
		List<UserCategory> newUserCategoryList =  new ArrayList<>();
		
		for (Integer userCatId : userCategoryList) {
			UserCategory userCategoryDB = userCategoryDao.findUserCategoryById(userCatId);
			newUserCategoryList.add(userCategoryDB);
		}
		
		User newUser = new User(id, newUserCategoryList);

		User userCreated = userDao.addUser(newUser);

		if (userCreated != null) {
			return userCreated;
		}
		
		return null;

	}

}
