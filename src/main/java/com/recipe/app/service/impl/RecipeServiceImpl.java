package com.recipe.app.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.recipe.app.dao.RecipeDao;
import com.recipe.app.dao.RecipeIngredientDao;
import com.recipe.app.dao.UserCategoryDao;
import com.recipe.app.entity.Recipe;
import com.recipe.app.entity.RecipeIngredient;
import com.recipe.app.entity.User;
import com.recipe.app.entity.UserCategory;
import com.recipe.app.service.RecipeService;
import com.recipe.app.utils.CompareObjects;

@Service
public class RecipeServiceImpl implements RecipeService {

	private Random randomGenerator;

	@Autowired
	private RecipeDao recipeDao;

	@Autowired
	private RecipeIngredientDao recipeIngredientDao;

	@Autowired
	private UserCategoryDao userCategoryDao;

	@Override
	public List<Recipe> getAllRecipes() {
		// TODO Auto-generated method stub
		List<Recipe> recipeList = recipeDao.getAllRecipes();
		Recipe rec = recipeList.get(0);
		System.out.println(
				"A recipe list size é : " + rec.getNameRecipe() + " , size - " + rec.getIngredientList().size());
		System.out.println("receita :" + rec);
		// System.out.println("user category list " +
		// rec.getUserCatagoryList().size());
		List<UserCategory> listCat = rec.getUserCategoryList();
		// for (UserCatagory userCatagory : listCat) {
		// System.out.println("name user categoria da receita -> "
		// +rec.getNameRecipe()+ " - "+ userCatagory.getNameUserCategory());
		// }

		return recipeList;
	}

	@Override
	public Recipe addRecipe(Recipe recipe) {
		// TODO Auto-generated method stub
		Recipe newRecipe = recipeDao.addRecipe(recipe);
		return newRecipe;
	}

	@Override
	public boolean deleteRecipeByName(String name) {
		// TODO Auto-generated method stub

		boolean deleted = recipeDao.deleteRecipeByName(name);

		return deleted;
	}

	@Override
	public Recipe findRecipeByName(String recipeName) {
		Recipe recipeDb = recipeDao.findRecipeByName(recipeName);

		return recipeDb;
	}

	@Override
	public List<Recipe> getRecipesByNum(List<Integer> idListCategory, int numOfRecipes) {

		ArrayList<List<Recipe>> listOfRecipesByIdUserCategory = new ArrayList<List<Recipe>>();
		List<Recipe> listOfRandomRecipes = new ArrayList<Recipe>();

		for (int i = 0; i < idListCategory.size(); i++) {
			UserCategory userCateg = userCategoryDao.findUserCategoryById(idListCategory.get(i));
			List<Recipe> recipesFromUserCategory = userCateg.getRecipeList();
			listOfRecipesByIdUserCategory.add(recipesFromUserCategory);
		}

		randomGenerator = new Random();

		while (listOfRandomRecipes.size() < numOfRecipes) {
			int indexUserCateg = randomGenerator.nextInt(idListCategory.size());
			int indexRecipe = randomGenerator.nextInt(listOfRecipesByIdUserCategory.get(indexUserCateg).size());
			int indNewRecipe = listOfRecipesByIdUserCategory.get(indexUserCateg).get(indexRecipe).getIdRecipe();
			if (!CompareObjects.containsRecipeWithId(listOfRandomRecipes, indNewRecipe)) {
				listOfRandomRecipes.add(listOfRecipesByIdUserCategory.get(indexUserCateg).get(indexRecipe));

			}

		}
		System.out.println("num of recipes---> " + numOfRecipes);

		return listOfRandomRecipes;
	}

	@Override
	public Recipe findRecipeById(int idRecipe) {

		Recipe recipeDb = null;
		try {
			recipeDb = recipeDao.findRecipeById(idRecipe);

		} catch (Exception e) {
			throw e;
		}

		return recipeDb;
	}

	@Override
	public Recipe rateRecipe(float rating, int idRecipe) {

		Recipe recipeOld = null;
		Recipe recipeDb = null;

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date updateDate = new Date();
		
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate localDate = LocalDate.now();
		System.out.println(dtf.format(localDate)); //2016/11/16
		

		try {
			recipeOld = recipeDao.findRecipeById(idRecipe);
			// UPDATE RECIPE
			float newRate = rateCalc(rating, recipeOld.getRating(), recipeOld.getNumberOfRatings());
			recipeOld.setRating(Math.round(newRate));
			recipeOld.setNumberOfRatings(recipeOld.getNumberOfRatings() + 1);
			recipeOld.setUpdateDate(java.sql.Date.valueOf(localDate));

		} catch (Exception e) {
			throw e;
		}

		try {
			// recipe updated
			recipeDb = recipeDao.updateRecipe(recipeOld);

		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}

		

		return recipeDb;
	}

	private float rateCalc(float newRating, Integer ratingOld, Integer numberOfRatings) {

		float newRate = 3;

		try {
			newRate = (ratingOld + newRating) / 2;
		} catch (ArithmeticException e) {
			// TODO: handle exception
			throw e;
		}

		return newRate;

	}

	@Override
	public List<Recipe> findListRecipesByIds(List<Integer> idListRecipes) {
		List<Recipe> listRecipes = new ArrayList<>();
		for (int i = 0; i < idListRecipes.size(); i++) {
			Recipe favouriteRecipe = findRecipeById(idListRecipes.get(i));
			listRecipes.add(favouriteRecipe);
		}
		return listRecipes;
	}

}
