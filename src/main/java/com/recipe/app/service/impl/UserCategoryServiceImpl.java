package com.recipe.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.recipe.app.dao.RecipeDao;
import com.recipe.app.dao.UserCategoryDao;
import com.recipe.app.dao.UserDao;
import com.recipe.app.entity.User;
import com.recipe.app.entity.UserCategory;
import com.recipe.app.service.UserCategoryService;

@Service
public class UserCategoryServiceImpl implements UserCategoryService {

	@Autowired
	private UserCategoryDao userCategoryDao;
	
	@Autowired
	private UserDao userDao;

	@Override
	public List<UserCategory> findAllUserCategory() {
		// TODO Auto-generated method stub

		List<UserCategory> listUserCat = userCategoryDao.findAllUserCategory();

		return listUserCat;
	}

	@Override
	public boolean deleteUserCategoryByName(String name) {
		// TODO Auto-generated method stub

		boolean deleted = userCategoryDao.deleteUserCategoryByName(name);

		return deleted;
	}

	@Override
	public boolean updateUserCategory(String nameCategoryOld,String nameCategoryNew) {
		// TODO Auto-generated method stub
		UserCategory userCatdb = userCategoryDao.findUserCategoryByName(nameCategoryOld);
		userCatdb.setNameUserCategory(nameCategoryNew);
		UserCategory userCat = userCategoryDao.updateUserCategory(userCatdb);
		if (userCat != null) {
			return true;
		}
		return false;
	}

	@Override
	public UserCategory findByName(String nameUserCat) {
		// TODO Auto-generated method stub
		UserCategory userCateg = userCategoryDao.findUserCategoryByName(nameUserCat);
		
		return userCateg;
	}

	@Override
	public UserCategory addUserCategory(String newCategory) {
		UserCategory newUserCategory= new UserCategory(newCategory);
		// TODO Auto-generated method stub
		boolean addUser = userCategoryDao.addUserCategory(newUserCategory);
		
		if (addUser) {
			return newUserCategory;
		}
		return null;
	}

	@Override
	public List<UserCategory> findUserCategoryByUserId(int userId) {
		User userDB = userDao.findUserById(userId);
		
		
		List<UserCategory> listOfUserCategory = userDB.getUserCategoryList();
		return listOfUserCategory;
	}

	@Override
	public UserCategory findUserCategoryById(int idUserCategory) {
		UserCategory userCategorydb = userCategoryDao.findUserCategoryById(idUserCategory);

		return userCategorydb;
	}



}
