package com.recipe.app.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.recipe.app.dao.CategoryADao;
import com.recipe.app.dao.CategoryBDao;
import com.recipe.app.dao.IngredientDao;
import com.recipe.app.dao.MeasureDao;
import com.recipe.app.dao.RecipeDao;
import com.recipe.app.dao.RecipeIngredientDao;
import com.recipe.app.dao.UserCategoryDao;
import com.recipe.app.entity.CategoryA;
import com.recipe.app.entity.CategoryB;
import com.recipe.app.entity.Ingredient;
import com.recipe.app.entity.IngredientHelper;
import com.recipe.app.entity.Measure;
import com.recipe.app.entity.Recipe;
import com.recipe.app.entity.RecipeIngredient;
import com.recipe.app.entity.UserCategory;
import com.recipe.app.service.IngredientService;
import com.recipe.app.utils.CompareObjects;

@Service
public class IngredienteServiceImpl implements IngredientService {

	@Autowired
	private IngredientDao ingredientDao;

	@Autowired
	private CategoryBDao categoryB_Dao;

	@Autowired
	private CategoryADao categoryA_Dao;

	@Autowired
	private MeasureDao measureDao;

	@Autowired
	private RecipeIngredientDao recipeIngredientDao;

	@Autowired
	private RecipeDao recipeDao;
	
	
	@Autowired
	private UserCategoryDao userCategoryDao;
	
	// _____________________________________________________________________________________________________________
	// ___________________________________ Ingredient
	// _____________________________________________________________
	// _____________________________________________________________________________________________________________

	@Override
	public List<Ingredient> findAllIngredients() {
		List<Ingredient> ingredientList = ingredientDao.findAllIngredients();

		// List<RecipeIngredient> recipeIngredientList =
		// ingredientList.get(0).getRecipeList();
		//// System.out.println("---- >Category ingredientB do ingredient, " +
		// ingredientList.get(0).getIngredientName()
		//// + " " + ingredientList.get(0).getCategorieIngredientB());
		// for (RecipeIngredient recipe : recipeIngredientList) {
		// System.out.println(
		// "A quantidade e receita é : " + recipe.getQuantity() + ", " +
		// recipe.getRecipe().getNameRecipe()
		// + " do ingrediente : " + ingredientList.get(0).getIngredientName()
		// + " tamanho lista de receitas associado : " +
		// recipeIngredientList.size());
		//
		// }
		return ingredientList;
	}

	@Override
	public Ingredient findIngredientById(Integer idIngredient) {
		Ingredient ingredient_db = ingredientDao.findIngredientById(idIngredient);

		return ingredient_db;
	}

	@Override
	public Ingredient findIngredientByName(String ingredient) {
		Ingredient ingredient_db = ingredientDao.findIngredientByName(ingredient);
		return ingredient_db;
	}

	@Override
	public Ingredient addIngredient(Ingredient ingredient) {

		return ingredientDao.addIngredient(ingredient);
	}

	@Override
	public void updateIngredient(Ingredient ingredient) {
		ingredientDao.updateIngredient(ingredient);
	}

	@Override
	public boolean deleteIngredient(int idIngredient) {
		return ingredientDao.deleteIngredient(idIngredient);

	}

	@Override
	public List<RecipeIngredient> findRecipesByIngredient(int idIngredient) {

		Ingredient ingredient_db = findIngredientById(idIngredient);

		List<RecipeIngredient> recipeList = ingredient_db.getRecipeList();

		for (RecipeIngredient recipe : ingredient_db.getRecipeList()) {
			System.out.println("---------------> " + recipe);
		}
		return recipeList;
	}

	// _____________________________________________________________________________________________________________
	// ___________________________________ CategoryA
	// _____________________________________________________________
	// _____________________________________________________________________________________________________________

	@Override
	public List<CategoryA> findAllCategoriesA() {
		List<CategoryA> listCategoriesA = categoryA_Dao.findAllCategoriesA();

		List<CategoryB> listaCatB = listCategoriesA.get(0).getListCategoriesB();
		// System.out.println("A LISTA DE CATEGORIAS A SAO ----> "+
		// listCategoriesA.get(0).getListCategoriesB());
		//
		// for (CategoryB categoryB : listaCatB) {
		// System.out.println("A CAT B É ----> "+categoryB);
		// }
		return listCategoriesA;
	}

	@Override
	public CategoryA findCategoryAById(int idCategoryA) {
		CategoryA categoryA = categoryA_Dao.findCategoryAById(idCategoryA);
		return categoryA;
	}

	@Override
	public CategoryA findCategoryAByName(String nameCategoryA) {
		CategoryA categoryA = categoryA_Dao.findCategoryAByName(nameCategoryA);
		return categoryA;
	}

	@Override
	public CategoryA addCategoryA(CategoryA newCategoryA) {
		return categoryA_Dao.addCategoryA(newCategoryA);
	}

	@Override
	public CategoryA updateCategoryA(int idCategoryA, CategoryA newCategoryA) {
		return categoryA_Dao.updateCategoryA(idCategoryA, newCategoryA);

	}

	@Override
	public boolean deleteCategoryA(int idCategoryA) {
		return categoryA_Dao.deleteCategoryA(idCategoryA);
	}

	// _____________________________________________________________________________________________________________
	// ___________________________________ CategoryB
	// _____________________________________________________________
	// _____________________________________________________________________________________________________________

	@Override
	public List<CategoryB> findAllCategoriesB() {
		List<CategoryB> listCategoriesB = categoryB_Dao.findAllCategoriesB();
		return listCategoriesB;
	}

	@Override
	public CategoryB findCategoryBById(int idCategoryB) {
		CategoryB categoryB = categoryB_Dao.findCategoryBById(idCategoryB);
		return categoryB;
	}

	@Override
	public CategoryB findCategoryBByName(String nameCategoryB) {
		CategoryB categoryB = categoryB_Dao.findCategoryBByName(nameCategoryB);
		return categoryB;
	}

	@Override
	public CategoryB addCategoryB(CategoryB newCategoryB) {
		return categoryB_Dao.addCategoryB(newCategoryB);

	}

	@Override
	public CategoryB updateCategoryB(CategoryB newCategoryB) {

		CategoryA newCategA = findCategoryAByName(newCategoryB.getCategoryIngredientA().getNameCategoryIngredientA());

		return newCategA == null ? null : categoryB_Dao.updateCategoryB(newCategoryB, newCategA);

	}

	@Override
	public boolean deleteCategoryB(int idCategoryB) {
		return categoryB_Dao.deleteCategoryB(idCategoryB);
	}

	// _____________________________________________________________________________________________________________
	// ___________________________________ Measure
	// _____________________________________________________________
	// _____________________________________________________________________________________________________________

	@Override
	public List<Measure> findAllMeasures() {
		List<Measure> measureList = measureDao.findAllMeasures();
		return measureList;
	}

	@Override
	public Measure findMeasureById(int idMeasure) {
		return measureDao.findMeasureById(idMeasure);

	}

	@Override
	public Measure findMeasureByName(String nameMeasure) {

		return measureDao.findMeasureByName(nameMeasure);

	}

	@Override
	public Measure updateMeasure(Measure newMeasure) {
		return measureDao.updateMeasure(newMeasure);

	}

	@Override
	public Measure addMeasure(Measure newMeasure) {
		return measureDao.addMeasure(newMeasure);

	}

	@Override
	public boolean deleteMeasure(int idMeasure) {
		return measureDao.deleteMeasure(idMeasure);

	}

	@Override
	public List<Recipe> findRecipesByIngredients(List<Integer> ingredientsIdChoosenList, int maxRecipes , List<UserCategory> listUserCategory) {
		
	

		List<Recipe> recipeListResult = new ArrayList<Recipe>();

		Ingredient ingredient_db;
		List<RecipeIngredient> recipeList = new ArrayList<RecipeIngredient>();

		for (Integer ingredientId : ingredientsIdChoosenList) {

			ingredient_db = findIngredientById(ingredientId);

			recipeList = ingredient_db.getRecipeList();

			for (RecipeIngredient recipeIngredient : recipeList) {
				//check if recipe has one of the users category passed as parameter
				List<UserCategory> userCatListFromRecipe = recipeIngredient.getRecipe().getUserCategoryList();
				//disjoint check if no elements in common
				if (Collections.disjoint(userCatListFromRecipe, listUserCategory)) {
					// if no elements in common continue
					continue;
				}
				// if recipe doesnt exist in result list ,adds the recipe
				if (!(CompareObjects.containsRecipeWithId(recipeListResult, recipeIngredient.getRecipe().getIdRecipe()))
						&& recipeListResult.size() < maxRecipes) {
					recipeListResult.add(recipeIngredient.getRecipe());
				}

			}
		}
		return recipeListResult;
	}
	
	

	@Override
	public List<IngredientHelper> findIngredientsByRecipe(int idRecipe) {

		List<RecipeIngredient> recipeIngredients = null;

		List<IngredientHelper> ingredientsList = new ArrayList<IngredientHelper>();

		Recipe recipeDb = null;
		try {
			recipeDb = recipeDao.findRecipeById(idRecipe);

		} catch (Exception e) {
			throw e;
		}

		try {
			recipeIngredients = recipeIngredientDao.findIngredientsByRecipe(recipeDb);

			if (!recipeIngredients.isEmpty()) {
				for (RecipeIngredient recipeIngredient : recipeIngredients) {
					// HAD TO DO THIS BECAUSE COULDNT PASS PROPER OBJECT
					// RecipeIngredient TO ANDROID
					Ingredient ingredient = recipeIngredient.getIngredient();
					Integer id = ingredient.getIdIngredient();
					String name = ingredient.getIngredientName();
					Measure measure = ingredient.getMeasure();
					int quantit = recipeIngredient.getQuantity();

					IngredientHelper ingredientHelper = new IngredientHelper(id, name, measure, quantit);
					ingredientsList.add(ingredientHelper);

				}
			}

		} catch (Exception e) {

			throw e;
		}

		return ingredientsList;
	}

	@Override
	public List<IngredientHelper> findIngredientstTotalByRecipesIds(List<Integer> listIdRecipes) {
		
		// for each recipe and sum the identic ingredients. return list Ingredients total 	
		List<IngredientHelper> ingredientListTotal = new ArrayList<>();
		for (Integer idRecipe : listIdRecipes) {
			List<IngredientHelper> ingredientList = findIngredientsByRecipe(idRecipe);
			mergeIngredientsList(ingredientListTotal,ingredientList);
		}
		
		return ingredientListTotal;
	}

	private void mergeIngredientsList(List<IngredientHelper> ingredientListTotal,
			List<IngredientHelper> ingredientNewList) {
		// TODO Auto-generated method stub
		for (IngredientHelper ingredient : ingredientNewList) {
			//verify if ingredient exists on totalList and returns index
			int indexOfIngredient = CompareObjects.containsIngredientHelperWithId(ingredientListTotal, ingredient.getIdIngredient());
			// get existting ingredient and sum its quantities and set TotalList
			if (indexOfIngredient != -1) {
				int quantityOld = ingredientListTotal.get(indexOfIngredient).getQuantity();
				int quantityToSum = ingredient.getQuantity();
				int quantityNewTotal = quantityOld + quantityToSum;
				
				ingredientListTotal.get(indexOfIngredient).setQuantity(quantityNewTotal);	
			}
			// if ingredient doesnt exist . adds to total list 
			else if(indexOfIngredient == -1){
				ingredientListTotal.add(ingredient);
			}
		}
		
		
	}

}
