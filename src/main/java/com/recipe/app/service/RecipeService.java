package com.recipe.app.service;

import java.util.List;

import com.recipe.app.entity.Recipe;
import com.recipe.app.entity.RecipeIngredient;

public interface RecipeService {

	List<Recipe> getAllRecipes();

	Recipe addRecipe(Recipe recipe);

	boolean deleteRecipeByName(String name);

	Recipe findRecipeByName(String recipeName);

	List<Recipe> getRecipesByNum(List<Integer> idListCategory, int numOfRecipes);

	Recipe findRecipeById(int idRecipe);

	Recipe rateRecipe(float rating, int idRecipe);

	List<Recipe> findListRecipesByIds(List<Integer> idListRecipes);

	

}
