package com.recipe.app.service;

import java.util.List;

import com.recipe.app.entity.User;
import com.recipe.app.entity.UserCategory;

public interface UserService {

	List<User> findAllUsers();

	boolean deleteUserById(int id);

	User addUser(int id, List<UserCategory> userCategoryList);

	User findUserById(int id);

	User updateUser(int idUser, List<Integer> userCategoryIdList);

	User addUser(List<Integer> userCategoryList);


}
