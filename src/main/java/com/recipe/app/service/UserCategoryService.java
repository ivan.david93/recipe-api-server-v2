package com.recipe.app.service;

import java.util.List;


import com.recipe.app.entity.UserCategory;


public interface UserCategoryService {

	public List<UserCategory> findAllUserCategory();

	public boolean deleteUserCategoryByName(String name);

	boolean updateUserCategory(String nameCategoryOld, String nameCategoryNew);

	public UserCategory findByName(String nameUserCat);

	public UserCategory addUserCategory(String newCategory);

	

	public List<UserCategory> findUserCategoryByUserId(int userId);

	public UserCategory findUserCategoryById(int idUserCategory);


	

	
}
