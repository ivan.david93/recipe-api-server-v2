package com.recipe.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.recipe.app.entity.CategoryA;
import com.recipe.app.entity.CategoryB;
import com.recipe.app.entity.Ingredient;
import com.recipe.app.entity.IngredientHelper;
import com.recipe.app.entity.Measure;
import com.recipe.app.entity.Recipe;
import com.recipe.app.entity.RecipeIngredient;
import com.recipe.app.entity.UserCategory;

public interface IngredientService {

	// _____________________________________________________________________________________________________________
	// ___________________________________ Ingredient
	// _____________________________________________________________
	// _____________________________________________________________________________________________________________

	List<Ingredient> findAllIngredients();

	Ingredient findIngredientById(Integer idIngredient);

	Ingredient findIngredientByName(String ingredientName);

	Ingredient addIngredient(Ingredient ingredient);

	void updateIngredient(Ingredient ingredient);

	boolean deleteIngredient(int idIngredient);

	List<RecipeIngredient> findRecipesByIngredient(int idIngredient);

	// _____________________________________________________________________________________________________________
	// ___________________________________ CategoryA
	// _____________________________________________________________
	// _____________________________________________________________________________________________________________

	List<CategoryA> findAllCategoriesA();

	CategoryA findCategoryAById(int idCategoryA);

	CategoryA findCategoryAByName(String nameCategoryA);

	CategoryA updateCategoryA(int idCategoryA, CategoryA newCategoryA);

	CategoryA addCategoryA(CategoryA newCategoryA);

	boolean deleteCategoryA(int idCategoryA);

	// _____________________________________________________________________________________________________________
	// ___________________________________ CategoryB
	// _____________________________________________________________
	// _____________________________________________________________________________________________________________

	List<CategoryB> findAllCategoriesB();

	CategoryB findCategoryBById(int idCategoryB);

	CategoryB findCategoryBByName(String nameCategoryB);

	CategoryB updateCategoryB(CategoryB newCategoryB);

	CategoryB addCategoryB(CategoryB newCategoryB);

	boolean deleteCategoryB(int idCategoryB);

	// _____________________________________________________________________________________________________________
	// ___________________________________ Measure
	// _____________________________________________________________
	// _____________________________________________________________________________________________________________

	List<Measure> findAllMeasures();

	Measure findMeasureById(int idMeasure);

	Measure findMeasureByName(String nameMeasure);

	Measure updateMeasure(Measure newMeasure);

	Measure addMeasure(Measure newMeasure);

	boolean deleteMeasure(int idMeasure);

	List<Recipe> findRecipesByIngredients(List<Integer> ingredientsChoosenList,int maxRecipes, List<UserCategory> userCatList);

	List<IngredientHelper> findIngredientsByRecipe(int idRecipe);

	List<IngredientHelper> findIngredientstTotalByRecipesIds(List<Integer> listIdRecipes);

}
