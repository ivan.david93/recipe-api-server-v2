/**
 * 
 */

// criacao dos controladores
appRecipe.controller("loginController", function($scope, $http) {

	$scope.user = {};

	$scope.autenticar = function() {
		console.log("Chamou autenticar : " + $scope.user.name + " - "
				+ $scope.user.password);
	
	
	
		$scope.toggleSignIn($scope.user.name, $scope.user.password);
	}
	 /**
		 * Handles the sign in button press.
		 */
    $scope.toggleSignIn = function(username,password) {
      if (firebase.auth().currentUser) {
        // [START signout]
        firebase.auth().signOut();
        // [END signout]
      } else {
        var email = username;// document.getElementById('email').value;
        var password = password;// document.getElementById('password').value;
        if (email.length < 4) {
          alert('Please enter an email address.');
          return;
        }
        if (password.length < 4) {
          alert('Please enter a password.');
          return;
        }
        // Sign in with email and pass.
        // [START authwithemail]
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(function(firebaseUser) {
        	console.log("Success user -->>>");
        	console.log(firebaseUser);
        	$scope.autenticarServer(email, password);
        	// usar token da resposta
        	console.log($scope.token);
        	
        	localStorage.setItem("userToken",$scope.token );
        	
            // Success
        })
       .catch(function(error) {
            // Error Handling
    	   // Handle Errors here.
           var errorCode = error.code;
           var errorMessage = error.message;
           // [START_EXCLUDE]
           if (errorCode === 'auth/wrong-password') {
             alert('Wrong password.');
           } else {
             alert(errorMessage);
           }
           console.log(error);
           // document.getElementById('quickstart-sign-in').disabled = false;
           // [END_EXCLUDE]
       });
        
        
      }// end else
    }
    
    
	
// // Simple POST request example:
   $scope.autenticarServer = function(user,pass) {
	   
	 var config = {
               headers : {
                   'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
               }
          };
	   
	 var data = {"userName":user,"password":pass};
	 $http({method : 'POST',
	 url : 'http://localhost:8080/recipe/autenticar',data ,config
	 }).then(function successCallback(response) {
	 // this callback will be called asynchronously
	 // when the response is available
	 console.log("Sucesso autenticar");
	 console.log(response);
	 $scope.token = response.data.token;
	 }, function errorCallback(response) {
	 // called asynchronously if an error occurs
	 // or server returns response with an error status.
	 console.log("Fail autenticar " + response);
	 }); 
	   
   }
   


});