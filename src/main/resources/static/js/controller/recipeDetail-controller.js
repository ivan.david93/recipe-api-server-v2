/**
 * 
 */

/**
 * 
 */
// criacao dos controladores
appRecipe.controller("recipeDetailController", function($scope, $timeout,$routeParams,
		$mdSidenav, $log, $http) {
	
	var nomeReceita =  $routeParams;

	var recipe = 'Arroz%20de%20Pato%20com%20Bacon%20e%20Laranja';
	$scope.receitas = {};
	$scope.receita = [];

	$scope.getReceita = function(receita) {
		// Simple GET request example:
		$http({
			method : 'GET',
			url : 'http://localhost:8080/recipe/findRecipeByName/' + receita
		}).then(function successCallback(response) {
			// this callback will be called asynchronously
			// when the response is available
			console.log(response.data);
			console.log(response.status);
			$scope.receita = response.data;
		}, function errorCallback(response) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			console.log(response.data);
			console.log(response.status);
		});
	}
	
	$scope.getReceita(recipe);

});