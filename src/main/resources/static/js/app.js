//criar modulo principal da aplicacao
var appRecipe = angular.module("appRecipe", [ 'ngMaterial', 'ngRoute','ngAnimate','ngAria','ngMessages','ngMdIcons']);

appRecipe.config(function($routeProvider,$locationProvider) {
	
	
	

	$routeProvider
	.when('/dashboard/recipe', {
		templateUrl : 'view/recipe.html',
		controller : 'recipeController'
	})
	.when('/recipe/:idRecipe', {
		templateUrl : 'view/recipeDetail.html',
		controller : 'recipeDetailController'
	})
	.when('/dashboard/category', {
		templateUrl : 'view/category.html',
		controller : 'categoryController'
	})
	.when('/dashboard/login', {
		templateUrl : 'view/login.html',
		controller : 'loginController'
	}).otherwise({
		rediretTo : '/'
	});
	
	
	
	
	// configure html5 to get links working on jsfiddle
	  $locationProvider.html5Mode(true);

});

appRecipe.config(function($httpProvider){
	$httpProvider.interceptors.push("tokenInterceptor");
});
